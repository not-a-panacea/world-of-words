﻿using System;
using UniRx;
using UnityEngine;
using TutorialSystem;

namespace Core {
    public class GameResultHandler : MonoBehaviour {
        [SerializeField] private GameInitializer _gameInitializer = null;
        [SerializeField] private GameController _gameController = null;
        [SerializeField] private GameObject _endGameScreen = null;

        private GameData _gameData;

        private void Awake() {
            _gameData = GameData.Current;
            _endGameScreen.SetActive(false);

            if (Tutorial.IsCompleted) {
                _gameController.GameCompleted += OnGameCompleted;
            }
            else {
                _gameController.GameCompleted += OnTutorialGameCompleted;
            }
        }

        private void OnGameCompleted(GameResult result) {
            _endGameScreen.SetActive(true);

            if (result.Sucsses) {
                result.GameArgs.Profile.AddProgress(result.GameArgs.Category.Id);

                Observable.Timer(TimeSpan.FromSeconds(2)).Subscribe(_ => {
                    bool isCompleted = _gameData.IsCategoryCompleted(result.GameArgs.Category);
                    if (isCompleted) {
                        BackToMainMenu();
                    }
                    else {
                        var gameArgs = _gameData.GetStartArgs(result.GameArgs.Category);
                        _gameInitializer.RestartGame(gameArgs);
                    }
                });
            }
        }

        private void OnTutorialGameCompleted(GameResult result) {
            _endGameScreen.SetActive(true);

            if (result.Sucsses) {
                result.GameArgs.Profile.AddProgress(result.GameArgs.Category.Id);

                Observable.Timer(TimeSpan.FromSeconds(2)).Subscribe(_ => {
                    try {
                        var gameArgs = TutorialSystem.CrosswordTutorial.GetGameArgs();
                        _gameInitializer.RestartGame(gameArgs);
                    }
                    catch (Exception ex) {
                        Debug.LogException(ex);
                        BackToMainMenu();
                    }
                });
            }
        }

        private void Update() {
            if (Input.GetKeyUp(KeyCode.Escape)) {
                BackToMainMenu();
            }
        }

        private void BackToMainMenu() {
            SceneLoader.LoadMainMenu(() => {
                SceneLoader.UnloadGame();
            });
        }
    }
}