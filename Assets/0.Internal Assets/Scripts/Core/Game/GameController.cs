using Presentors;
using System;
using System.Linq;
using UnityEngine;
using UniRx;
using Model;
using ExperienceSystem;
using Hints;
using System.Collections.Generic;
using Common.Types;

namespace Core {
    public class GameController : MonoBehaviour, IGameField, IDisposable {
        [SerializeField] private LettersGrid _grid = null;
        [SerializeField] private LetterSelector _selector = null;
        [SerializeField] private CoinsPlacer _coinsPlacer = null;
        [SerializeField] private HintsPresentor _hintsPresentor = null;

        public event Action<GameResult> GameCompleted;

        private ExperienceCalculator _experienceCalculator;
        private LevelProgress _gameData;
        private GameStartArgs _args;
        private Crossword _crossword => _args.Level.Crossword;

        public void Init(GameStartArgs args) {
            _args = args;
            _gameData = GameProgress.Current.GetProgress(_args);

            _grid.Init(_args.Level.Crossword.Grid);
            _grid.ShowPositions(_gameData.GetOpenedPosition().ToArray());

            _selector.Init(_args.Level.Crossword.Letters);
            _selector.CombinationSelected += OnCombinationSelected;

            _experienceCalculator = new ExperienceCalculator(_crossword.Words.Select(wd => wd.Word.Length), _args.Level.Exp);
            InitCoins();

            _hintsPresentor.Assign(HintsStorage.Instance.GetContent());

            this.AddTo(this);
        }

        private void InitCoins() {
            if (_args.Level.HasCoins) {
                var word = _crossword.Words.Select(wd => wd.Word).Max();
                var wordData = _crossword.Words.FirstOrDefault(wd => wd.Word == word);
                Observable.NextFrame().Subscribe(_ => {
                    _coinsPlacer.Init(wordData.LetterPositions.Where(p => _gameData.IsOpen(p) == false).ToArray());
                });
            }
            else {
                _coinsPlacer.Hide();
            }
        }

        private void OnCombinationSelected(char[] combination) {
            var word = string.Join("", combination);
            Debug.Log("Selected word: " + word);

            if (_gameData.TryOnpenWord(word, out var wordData)) {
                OnWordFinded(wordData);
            }
            else {
                Debug.Log("Word already funded or doesn't exist: " + word);
            }
        }

        private void OnWordFinded(Crossword.WordData wordData) {
            _grid.ShowPositions(wordData.LetterPositions);
            _coinsPlacer.PositionsOpened(wordData.LetterPositions);
            Experience.Add(_experienceCalculator.GetExpAtWordLenght(wordData.Word.Length));
            Debug.Log("Word opened: " + wordData.Word);

            if (_gameData.AllWordsFounded) {
                Debug.Log("You win!");
                var result = new GameResult(_args, true);
                GameCompleted?.Invoke(result);
            }
        }

        public void Dispose() {
            GameProgress.Current.Save();
        }

        #region IGameField
        public IEnumerable<GameWord> GetGameData() {
            return _gameData.GetGameWords();
        }

        public void OpenPositions(params Vector2IntSimple[] positions) {
            var result = _gameData.OpenPositions(positions);
            foreach (var pair in result) {
                if (pair.completlyOpen) {
                    OnWordFinded(pair.wordData);
                }
                else {
                    _grid.ShowPositions(positions);
                    _coinsPlacer.PositionsOpened(positions);
                }
            }
        }
        #endregion
    }
}