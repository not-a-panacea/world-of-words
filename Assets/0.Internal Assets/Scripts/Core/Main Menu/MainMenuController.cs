using EconomySystem;
using Model;
using Presentors;
using UnityEngine;
using Core.TutorialSystem;

namespace Core {
    public class MainMenuController : MonoBehaviour {
        [SerializeField] private CategoriesList _categoriesList = null;
        [SerializeField] private GameInitializer _gameInitializer = null;

        private GameData _gameData;

        private void Start() {
            GameData.Init();
            _gameData = GameData.Current;

            _categoriesList.Init(GameData.Current.CategoriesData);
            _categoriesList.CategoryClicked += OnCategoryClicked;

            CrosswordTutorial.Start(_gameInitializer);
        }

        private void OnCategoryClicked(Category category) {
            bool isOpen = _gameData.IsCategoryOpened(category);
            if (isOpen == false) {
                if (category.OpeningType == OpeningType.Coins) {
                    var price = category.CreatePrice();
                    var result = Economy.ProvideTransaction(_gameData.Profile.ID, price.CreateTransaction());
                    if (result.Success) {
                        _gameData.Profile.CategoryOpen(category.Id);
                        _categoriesList.OnCategoryOpened(category);
                    }
                }

                if (category.OpeningType == OpeningType.ADS) {

                }
            }
            else if (_gameData.IsCategoryCompleted(category) == false) {
                StartGame(category);
            }
        }

        private void StartGame(Category category) {
            var gameArgs = _gameData.GetStartArgs(category);
            _gameInitializer.StartGame(gameArgs);
        }
    }
}
