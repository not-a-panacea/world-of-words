using Model;
using System;
using System.Linq;
using TutorialSystem;

namespace Core.TutorialSystem {
    public class CrosswordTutorial : ITutorialStepHandler {
        private static CrosswordTutorial _instance;

        private const string CategoryId = "Tutorial";

        private GameInitializer _gameInitializer;
        private bool _canStart = false;
        private Category _category;

        private CrosswordTutorial(GameInitializer gameInitializer) {
            Tutorial.Init();
            if (Tutorial.IsCompleted == false) {
                _canStart = true;
                _gameInitializer = gameInitializer;
            }
        }

        public void HandleTutorialStep(Tutorial.Step step, Action<bool, Tutorial.Step> handlingCompleted) {
            if (step == Tutorial.Step.Start || step == Tutorial.Step.CategoryPlay) {
                try {
                    GameStartArgs startArgs = GetGameArgs();
                    _gameInitializer.StartGame(startArgs, () => {
                        handlingCompleted(true, Tutorial.Step.CategoryPlay);
                    });
                }
                catch (Exception) {
                    _canStart = false;
                    handlingCompleted(true, Tutorial.Step.CategoryPassed);
                }
            }
            else {
                handlingCompleted(false, step);
            }
        }

        public static void Start(GameInitializer gameInitializer) {
            if (_instance == null) _instance = new CrosswordTutorial(gameInitializer);
            if (_instance._canStart) {
                _instance._category = GameDataLoader.LoadCategoriesData()
                    .FirstOrDefault(c => string.Compare(c.Id, CategoryId, true) == 0);
                if (_instance._category != null) {
                    Tutorial.Start();
                    Tutorial.Subscribe(_instance, Tutorial.Step.CategoryPlay);
                }
            }
        }

        public static bool IsCategoryCompleted(out int currentLevelIndex) {
            if (_instance._category == null) {
                throw new NullReferenceException("Tutorial category is null");
            }

            var levelsCount = GameDataLoader.LoadLevels(_instance._category.Id).Count;
            currentLevelIndex = Profile.Current.GetProgress(_instance._category.Id);
            return currentLevelIndex >= levelsCount;
        }

        public static GameStartArgs GetGameArgs() {
            if (_instance._category == null) {
                throw new NullReferenceException("Tutorial category is null");
            }
            else if (IsCategoryCompleted(out var currentLevelIndex) == false) {
                var level = GameDataLoader.LoadLevel(_instance._category.Id, currentLevelIndex);
                GameStartArgs startArgs = new GameStartArgs(Profile.Current, _instance._category, level);
                return startArgs;
            }
            else {
                throw new InvalidOperationException("Tutorial category is completed");
            }
        }
    }
}
