using System;
using UnityEngine;

namespace Core {
    public class GameInitializer : MonoBehaviour {

        private GameStartArgs _gameStartArgs;

        public void StartGame(GameStartArgs gameStartArgs, Action onGameLoaded = null) {
            LoadGame(gameStartArgs, SceneLoader.LoadGame, () => { 
                onGameLoaded?.Invoke(); SceneLoader.UnloadMainMenu(); 
            });
        }

        public void RestartGame(GameStartArgs gameStartArgs) {
            LoadGame(gameStartArgs, SceneLoader.ReloadGame, () => { });
        }

        private void LoadGame(GameStartArgs gameStartArgs, Action<Action> loadComand, Action callback) {
            transform.parent = null;
            DontDestroyOnLoad(gameObject);
            _gameStartArgs = gameStartArgs;

            loadComand?.Invoke(() => {
                OnGameLoaded();
                callback?.Invoke();
            } );
        }

        private void OnGameLoaded() {
            var gameController = FindObjectOfType<GameController>();
            gameController.Init(_gameStartArgs);
            Destroy(gameObject);
        }
    }
}