using Common.Loading;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core {
    public static class SceneLoader {
        private static LoadingScreen _loadingScreen { get; set; }
        private static List<string> _scenesInProgress = new List<string>();

        public static void LoadLoadingScreen(Action callback) {
            if (_loadingScreen) {
                callback.Invoke();
                return;
            }
            void newCallback() {
                _loadingScreen = GameObject.FindObjectOfType<LoadingScreen>();
                callback?.Invoke();
            }
            LoadScene(Constants.Scenes.Loading, LoadSceneMode.Single, false, newCallback);
        }

        public static void LoadMainMenu(Action callback) {
            LoadScene(Constants.Scenes.MainMenu, LoadSceneMode.Additive, true, callback);
        }

        public static void UnloadMainMenu(Action callback = null) {
            UnloadScene(Constants.Scenes.MainMenu, callback);
        }

        public static void LoadGame(Action callback) {
            LoadScene(Constants.Scenes.Game, LoadSceneMode.Additive, true, callback);
        }

        public static void UnloadGame(Action callback = null) {
            UnloadScene(Constants.Scenes.Game, callback);
        }

        public static void ReloadGame(Action callback) {
            ReloadScene(Constants.Scenes.Game, LoadSceneMode.Additive, callback);
        }

        public static void ReloadScene(string sceneName, LoadSceneMode mode, Action callback) {
            bool setAsActive = SceneManager.GetActiveScene().name == sceneName;

            UnloadScene(sceneName, () => {
                LoadScene(sceneName, mode, setAsActive, callback);
            });
        }

        public static void LoadScene(string sceneName, LoadSceneMode mode, bool setAsActive, Action callback) {
            AddSceneInProgress(sceneName);
            SceneManager.sceneLoaded += OnSceneLoaded;

            var loadingProcess = SceneManager.LoadSceneAsync(sceneName, mode);
            if (_loadingScreen) _loadingScreen.Show(loadingProcess);

            void OnSceneLoaded(Scene loadedScene, LoadSceneMode mode) {
                if (RemoveScene(loadedScene.name)) {
                    SceneManager.sceneLoaded -= OnSceneLoaded;
                    if (setAsActive) SceneManager.SetActiveScene(loadedScene);
                    callback?.Invoke();
                }
            }
        }

        public static void UnloadScene(string sceneName, Action callback) {
            AddSceneInProgress(sceneName);
            SceneManager.sceneUnloaded += OnSceneUnloaded;

            var loadingProcess = SceneManager.UnloadSceneAsync(sceneName);
            if (_loadingScreen) _loadingScreen.Show(loadingProcess);

            void OnSceneUnloaded(Scene unloadedScene) {
                if (RemoveScene(unloadedScene.name)) {
                    SceneManager.sceneUnloaded -= OnSceneUnloaded;
                    callback?.Invoke();
                }
            }
        }

        private static void AddSceneInProgress(string sceneName) {
            if (_scenesInProgress.Contains(sceneName)) {
                throw new InvalidOperationException("Scene already unloading");
            }
            _scenesInProgress.Add(sceneName);
        }

        private static bool RemoveScene(string name) {
            return _scenesInProgress.Remove(name);
        }
    }
}
