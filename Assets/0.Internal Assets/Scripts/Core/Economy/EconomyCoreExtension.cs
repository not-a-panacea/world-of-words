using EconomySystem;
using Model;

namespace Core {
    public static class EconomyCoreExtension {
        public static Price CreatePrice(this Category category) {
            var config = EconomyDataProvider.CurrencyConfig;
            var currency = config.GetCurrency(config.DefaultCurrencyId);
            return new Price(currency, category.Price);
        }
    }
}
