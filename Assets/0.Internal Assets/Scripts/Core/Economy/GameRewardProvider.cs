using EconomySystem;
using Presentors;
using Rewards;
using UnityEngine;

namespace Core.EconomySystem {
    public class GameRewardProvider : MonoBehaviour
    {
        [SerializeField] private Currency _currency = null;
        [SerializeField] private CoinsPlacer _coinsPlacer = null;

        private void Awake() {
            _coinsPlacer.CoinsFinded += OnCoinsFinded;
        }

        private void OnCoinsFinded(int coinsCoint) {
            var reward = CurrencyReward.Create(_currency, coinsCoint);
            reward.Gein();
        }
    }
}
