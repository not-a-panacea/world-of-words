using Model;

namespace Core {
    public readonly struct GameStartArgs {
        public readonly Profile Profile;
        public readonly Category Category;
        public readonly Level Level;

        public GameStartArgs(Profile profile, Category category, Level level) {
            Profile = profile;
            Category = category;
            Level = level;
        }
    }
}
