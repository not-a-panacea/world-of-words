namespace Core {
    public readonly struct GameResult {
        public readonly GameStartArgs GameArgs;
        public readonly bool Sucsses;

        public GameResult(GameStartArgs gameArgs, bool sucsses) {
            GameArgs = gameArgs;
            Sucsses = sucsses;
        }
    }
}
