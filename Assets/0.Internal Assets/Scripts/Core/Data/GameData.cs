﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core {
    public class GameData {
        private static GameData _current;
        public static GameData Current => _current ?? (_current = new GameData());
        public static void Init() {
            if (Current != null) {
                Debug.Log("Game data is loaded");
            } }


        private List<CategoryData> _categoriesData;
        private Profile _profile;

        private GameData() {
            _profile = Profile.Current;
            var categories = GameDataLoader.LoadCategoriesData()
                .Where(c => c.OpeningType != OpeningType.Unavailable)
                .ToList();
            categories.Sort();
            _categoriesData = new List<CategoryData>(categories.Count);


            foreach (var category in categories) {
                var data = new CategoryData();
                data.Category = category;
                data.LevelsCount = GameDataLoader.LoadLevels(category.Id).Count;
                data.CompletedLevels = _profile.GetProgress(category.Id);
                data.IsOpen = IsCategoryOpened(category);
                _categoriesData.Add(data);
            }

            _profile.CategoryOpened += OnCategoryOpened;
            _profile.LevelCompleted += OnLevelCompleted;
        }

        public Profile Profile => _profile;
        public IReadOnlyList<CategoryData> CategoriesData => _categoriesData;

        public GameStartArgs GetStartArgs(Category category) {
            var levelIndex = _profile.GetProgress(category.Id);
            var level = GameDataLoader.LoadLevel(category.Id, levelIndex);
            return new GameStartArgs(_profile, category, level);
        }

        public bool IsCategoryOpened(Category category) {
            return category.OpeningType == OpeningType.Opened || _profile.IsCategoryOpened(category.Id);
        }

        public bool IsCategoryCompleted(Category category) {
            return GetData(category.Id).IsCompleted;
        }

        private void OnLevelCompleted(string categoryId, int currentProgress) {
            UpdateData(categoryId, data => { data.CompletedLevels = currentProgress; return data; });
        }

        private void OnCategoryOpened(string categoryId) {
            UpdateData(categoryId, data => { data.IsOpen = true; return data; });
        }

        private void UpdateData(string categoryId, Func<CategoryData, CategoryData> updatater) {
            try {
                var data = GetData(categoryId);
                int index = _categoriesData.IndexOf(data);
                data = updatater(data);
                _categoriesData[index] = data;
            }
            catch (Exception) {
            }
        }

        private CategoryData GetData(string categoryId) {
            return _categoriesData.First(d => string.Compare(d.Category.Id, categoryId) == 0);
        }
    }
}
