﻿using System.Linq;
using System.Collections.Generic;
using UniRx;

namespace Core {
    public class GameProgress {
        public static GameProgress Current => _currner ?? (_currner = new GameProgress());
        private static GameProgress _currner;

        private List<LevelProgress> _progress;

        public GameProgress() {
            _progress = SaveLoader.LoadJsonToInternalStorage<List<LevelProgress>>(nameof(GameProgress));
            if (_progress == null) {
                _progress = new List<LevelProgress>();
            }
        }

        public LevelProgress GetProgress(GameStartArgs args) {
            var data = _progress.FirstOrDefault(d => string.Compare( d.CategoryId, args.Category.Id, true) == 0);
            if (data == null) {
                data = new LevelProgress(args.Category.Id, args.Level.Crossword);
                _progress.Add(data);
            }
            else {
                data.AssigneCrossword(args.Level.Crossword);
            }
            return data;
        }

        public void Save() {
            _progress.RemoveAll(gd => gd.AllWordsFounded);
            SaveLoader.SaveJsonToInternalStorage("", nameof(GameProgress), _progress);
        }
    }
}