﻿using System;
using System.Linq;
using System.Collections.Generic;
using UniRx;
using Model;
using Common.Types;
using static Model.Crossword;
using Common.Collections;
using FullSerializer;

namespace Core {
    public class LevelProgress {
        private Crossword _crossword;
        private HashSet<string> _foundedWords;
        [fsProperty] private string _categoryId;
        [fsProperty] private Table<bool> _openedPosition;

        public string CategoryId => _categoryId;
        public bool AllWordsFounded => _foundedWords.Count == _crossword.Words.Length;

        public LevelProgress(string categoryId, Crossword crossword) {
            _categoryId = categoryId;
            _crossword = crossword;
            _foundedWords = new HashSet<string>();
            var size = _crossword.Grid.Size;
            _openedPosition = new Table<bool>(size);
        }

        public void AssigneCrossword(Crossword crossword) {
            if (_crossword == null) {
                _crossword = crossword;
                _foundedWords = new HashSet<string>();
                foreach (var wordData in _crossword.Words) {
                    if (IsOpen(wordData)) {
                        _foundedWords.Add(wordData.Word);
                    }
                }
            }
        }

        public IEnumerable<GameWord> GetGameWords() {
            for (int i = 0; i < _crossword.Words.Length; i++) {
                var wd = _crossword.Words[i];
                bool[] openedPositions = GetPositionsState(wd.LetterPositions);
                yield return new GameWord(wd, openedPositions);
            }
        }

        public IEnumerable<Vector2IntSimple> GetOpenedPosition() {
            for (int i = 0; i < _openedPosition.Size.rows; i++) {
                for (int j = 0; j < _openedPosition.Size.colunms; j++) {
                    if (_openedPosition[i, j]) {
                        yield return new Vector2IntSimple(i, j);
                    }
                }
            }
        }

        public IEnumerable<Vector2IntSimple> GetClosedPosition() {
            for (int i = 0; i < _openedPosition.Size.rows; i++) {
                for (int j = 0; j < _openedPosition.Size.colunms; j++) {
                    if (_openedPosition[i, j] == false) {
                        yield return new Vector2IntSimple(i, j);
                    }
                }
            }
        }

        public IEnumerable<(WordData wordData, bool completlyOpen)> OpenPositions(params Vector2IntSimple[] positions) {
            for (int i = 0; i < positions.Length; i++) {
                var position = positions[i];

                if (_openedPosition[position.x, position.y]) {
                    throw new InvalidOperationException("Position already opened");
                }

                _openedPosition[position.x, position.y] = true;
            }
            
            var list = new List<(WordData wordData, bool openCompletly)>();
            var wordDatas = positions.SelectMany(position => _crossword.Words.Where(wd => wd.LetterPositions.Contains(position))).Distinct();

            foreach (var wordData in wordDatas) {
                if (_foundedWords.Contains(wordData.Word) == false) {
                    bool isOpenAtPositions = IsOpen(wordData);
                    if (isOpenAtPositions) _foundedWords.Add(wordData.Word);
                    list.Add((wordData, isOpenAtPositions));
                }
                else {
                    throw new InvalidOperationException("Detected closed position in open word");
                }
            }
            return list;
        }

        public bool TryOnpenWord(string word, out WordData wordData) {
            bool isExist = TryGetWordData(word, out wordData);
            if (isExist) {
                bool isOpen = _foundedWords.Add(word) == false && IsOpen(wordData);
                isExist &= isOpen == false;
                if (isOpen == false) {
                    for (int i = 0; i < wordData.LetterPositions.Length; i++) {
                        var position = wordData.LetterPositions[i];
                        _openedPosition[position.x, position.y] = true;
                    }
                }
            }
            return isExist;
        }

        public bool IsOpen(WordData wordData) {
            bool isOpen = true;
            for (int i = 0; i < wordData.LetterPositions.Length & isOpen; i++) {
                var position = wordData.LetterPositions[i];
                isOpen &= IsOpen(position);
            }
            return isOpen;
        }

        public bool IsOpen(Vector2IntSimple charPosition) {
            return _openedPosition[charPosition.x, charPosition.y];
        }

        public bool[] GetPositionsState(Vector2IntSimple[] charPositions) {
            bool[] result = new bool[charPositions.Length];
            for (int i = 0; i < charPositions.Length; i++) {
                result[i] = IsOpen(charPositions[i]);
            }
            return result;
        }

        public bool TryGetWordData(string word, out WordData wordData) {
            bool result = false;
            wordData = default;
            for (int i = 0; i < _crossword.Words.Length; i++) {
                wordData = _crossword.Words[i];
                if (string.Compare(wordData.Word, word, true) == 0) {
                    result = true;
                    break;
                }
            }
            return result;
        }
    }
}