using System.Collections.Generic;
using System.Linq;

namespace Core {
    public class ExperienceCalculator
    {
        private float _letterCoeff;

        public ExperienceCalculator(IEnumerable<int> words, int exp) {
            int sum = words.Sum();
            _letterCoeff = (float)exp / sum;
        }

        public float GetExpAtWordLenght(int wordLenght) {
            return _letterCoeff * wordLenght;
        }
    }
}
