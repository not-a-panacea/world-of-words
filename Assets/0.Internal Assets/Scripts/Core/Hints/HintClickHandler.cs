using Hints;
using Presentors;
using UnityEngine;

namespace Core {
    public class HintClickHandler : MonoBehaviour {
        [SerializeField] private GameController _gameController = null;
        [SerializeField] private HintsPresentor _hintsPresentor = null;

        private void OnEnable() {
            _hintsPresentor.HintClick += OnHintClick;
        }

        private void OnHintClick(Hint hint) {
            if (HintsStorage.Instance.GetCount(hint) > 0) {
                hint.Use(_gameController);
            }
            else {
                Debug.LogException(new System.InvalidOperationException("Need call ADS modul"));
            }
        }

        private void OnDisable() {
            _hintsPresentor.HintClick -= OnHintClick;
        }
    }
}
