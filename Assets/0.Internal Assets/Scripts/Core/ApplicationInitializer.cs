using Common.Serialization;
using System.Collections;
using UnityEngine;

namespace Core {
    public class ApplicationInitializer : MonoBehaviour {
        private void Awake() {
            Serialization.Init(SerializerType.FsSerializer);
        }

        private IEnumerator Start() {
            yield return null;
            SceneLoader.LoadLoadingScreen(OnLoadingLoaded);
        }

        private void OnLoadingLoaded() {
            SceneLoader.LoadMainMenu(()=> { });
        }
    }
}