using Common;
using Common.Views;
using ExperienceSystem;
using TMPro;
using UnityEngine;

namespace Views {
    public class RankView : View {
        [SerializeField] private TMP_Text _rankName = null;
        [SerializeField] private ProgressView _pregress = null;

        protected override void OnInit() {
            UpdateView();
            Experience.ExpCountChanged += OnExpCountChanged;
        }

        private void OnExpCountChanged((float prev, float current, float delta) args) {
            UpdateView();
        }

        private void UpdateView() {
            var level = Experience.GetCurrentLevel();
            var progress = Experience.GetProgress();

            _rankName.text = level.Name;
            _pregress.Value = progress;
        }

        protected override void OnDisposing() {
            Experience.ExpCountChanged -= OnExpCountChanged;
        }
    }
}
