using Common.Views;
using Model;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Views {
    public class CategoriesListView : View<IEnumerable<CategoryData>> {
        [SerializeField] private RectTransform _itemsParent = null;
        [SerializeField] private CategoryView _categoryViewPrefab = null;

        private List<CategoryView> _views;

        public event Action<CategoryView> CategoryClicked;

        protected override void OnAssign(IEnumerable<CategoryData> data) {
            _views = CreateCollection(data, _categoryViewPrefab, _itemsParent, OnViewCreated);
            _categoryViewPrefab.gameObject.SetActive(false);
        }

        private void OnViewCreated(CategoryView view) {
            view.Clicked += () => { CategoryClicked?.Invoke(view); };
        }

        public void OnCategoryOpened(Category category) {
            var view = _views.FirstOrDefault(v => v.Data.Category == category);
            if (view) {
                view.SetIsOpen();
            }
        }
    }
}
