using Common.Views;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Common;
using Model;

namespace Views {
    public class CategoryView : View<CategoryData> {
        [SerializeField] private Button _button = null;
        [SerializeField] private TMP_Text _nameText = null;
        [SerializeField] private TextProgressView _progressView = null;
        [SerializeField] private CategoryLockView _lockView = null;

        public event Action Clicked;

        protected override void OnInit() {
            _button.onClick.AddListener(OnButtonClicked);
        }

        protected override void OnAssign(CategoryData data) {
            _nameText.text = data.Category.Name;
            _progressView.MaxValue = data.LevelsCount;
            _progressView.Value = data.CompletedLevels;

            if (data.IsOpen) {
                _lockView.Hide();
            }
            else {
                _lockView.Assign(data.Category);
            }
        }

        private void OnButtonClicked() {
            Clicked?.Invoke();
        }

        public void SetIsOpen() {
            _lockView.Hide();
        }
    }
}
