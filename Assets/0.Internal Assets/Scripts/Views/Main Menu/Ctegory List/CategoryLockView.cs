using Common.Views;
using EconomySystem;
using Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Views {
    public class CategoryLockView : View<Category> {
        [SerializeField] private Image _lockIcon = null;
        [SerializeField] private TMP_Text _lockText = null;
        [SerializeField] private TMP_Text _lockPriceText = null;
        [Header("Money Price")]
        [SerializeField] private string _buyText = "";
        [SerializeField] private Currency _currency = null;
        [Space(20)]
        [Header("Ads")]
        [SerializeField] private Sprite _adsIcon = null;
        [SerializeField] private string _watchText = "";
        [SerializeField] private string _adsText = "";

        protected override void OnAssign(Category data) {
            switch (data.OpeningType) {
                case OpeningType.Unavailable:
                    break;
                case OpeningType.Opened:
                    Hide();
                    break;
                case OpeningType.Coins:
                    _lockIcon.sprite = _currency.Icon;
                    _lockText.text = _buyText;
                    _lockPriceText.text = data.Price.ToString();
                    Show();
                    break;
                case OpeningType.ADS:
                    _lockIcon.sprite = _adsIcon;
                    _lockText.text = _watchText;
                    _lockPriceText.text = _adsText;
                    Show();
                    break;
                default:
                    Hide();
                    break;
            }
        }
    }
}
