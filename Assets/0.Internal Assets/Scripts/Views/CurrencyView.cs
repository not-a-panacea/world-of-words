using Common.Views;
using EconomySystem;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Views {
    public class CurrencyView : View {
        [SerializeField] private string _profileId = "player_id";
        [SerializeField] private Currency _currency = null;
        [Space]
        [SerializeField] private Image _currencyIcon = null;
        [SerializeField] private TMPro.TMP_Text _currencyValue = null;

        protected override void OnInit() {
            _currencyIcon.sprite = _currency.Icon;

            Economy.TransactionProvided += OnTransactionProvided;

            DisplayAccountState();
        }

        private void OnTransactionProvided(string profileId, TransactionResult result) {
            if (string.Compare(_profileId, profileId, true) == 0 && result.Success) {
                DisplayAccountState();
            }
        }

        private void DisplayAccountState() {
            var account = Economy.GetAccounts(_profileId)
                    .FirstOrDefault(a => string.Compare(a.CurrencyID, _currency.Id) == 0);
            if (account.IsValid()) {
                UpdateValue(account.Value);
            }
            else {
                UpdateValue(0);
            }
        }

        private void UpdateValue(int value) {
            _currencyValue.text = value.ToString();
        }
    }
}
