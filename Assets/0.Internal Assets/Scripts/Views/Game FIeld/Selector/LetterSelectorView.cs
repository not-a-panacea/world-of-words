using Common;
using Common.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Presentors {
    public class LetterSelectorView : MonoBehaviour, IPointerUpHandler, IPointerDownHandler {
        [SerializeField] private RectLineRenderer _line = null;
        [SerializeField] private Color _activeLineColor = Color.white;
        [SerializeField] private Color _LineColor = Color.white;
        [SerializeField] private LetterItemView _letterView = null;

        public event Action<int> LetterSelected;
        public event Action SelectingEnded;

        private LetterItemView[] _letters;
        private LetterItemView _lastSelected;
        private Coroutine _lineUpdate;
        private Camera _camera;
        private Vector3 _pointerPosition;
        private bool _needStartDraw = false;

        private Stack<RectLineRenderer> _linesStorage = new Stack<RectLineRenderer>(5);
        private Stack<RectLineRenderer> _activeLines = new Stack<RectLineRenderer>(5);
        private HashSet<LetterItemView> _selectedItems = new HashSet<LetterItemView>();

        private void Awake() {
            _camera = Camera.main;
        }

        public void Init(char[] letters) {
            var count = letters.Length;

            _letters = new LetterItemView[count];
            _letters[0] = _letterView;

            var indexes = Enumerable.Range(0, count).ToArray();
            indexes.Shuffle();

            for (int i = 1; i < count; i++) {
                _letters[i] = Instantiate(_letterView, _letterView.transform.parent);
            }

            for (int i = 0; i < count; i++) {
                var letterView = _letters[i];
                letterView.Init(letters[i]);
                letterView.Rotate(360.0f / count * indexes[i]);

                letterView.Selected += () => OnLetterSelected(letterView);
                letterView.SelectingEnded += SlectiongEnd;
            }
        }

        public void ShuflLetters() {
            var count = _letters.Length;
            var indexes = Enumerable.Range(0, count).ToArray();
            indexes.Shuffle();
            for (int i = 0; i < count; i++) {
                var letterView = _letters[i];
                letterView.Rotate(360.0f / count * indexes[i]);
            }
        }

        private void OnLetterSelected(LetterItemView letter) {
            if (Input.GetMouseButton(0) == false) return;
            if (_selectedItems.Add(letter)) {
                if (_lastSelected == false) {
                    _needStartDraw = true;
                }

                if (_lastSelected && _lastSelected != letter) {
                    DrawLine(_lastSelected.Pivot.position, letter.Pivot.position);
                }

                letter.SetSelected();
                _lastSelected = letter;

                if (_needStartDraw) {
                    _line.Color = _activeLineColor;
                    _line.Show();
                    _lineUpdate = StartCoroutine(LineUpdate());
                    _needStartDraw = false;
                }

                LetterSelected?.Invoke(Array.IndexOf(_letters, letter));
            }
        }

        private IEnumerator LineUpdate() {
            while (true) {
                _pointerPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
                _line.Draw(_lastSelected.Pivot.position, _pointerPosition);
                yield return null;
            }
        }

        public void OnPointerUp(PointerEventData eventData) {
            SlectiongEnd();
        }

        public void OnPointerDown(PointerEventData eventData) {
        }

        private void SlectiongEnd() {
            _selectedItems.Clear();
            foreach (var letter in _letters) {
                letter.SetDeselected();
            }
            if (_lineUpdate != null) {
                StopCoroutine(_lineUpdate);
                _lineUpdate = null;
            } 
            _lastSelected = null;
            _line.Hide();
            FlushAllActiveLines();

            SelectingEnded?.Invoke();
        }

        private void FlushAllActiveLines() {
            while (_activeLines.Count > 0) {
                var line = _activeLines.Pop();
                line.Hide();
                _linesStorage.Push(line);
            }
        }

        private void DrawLine(Vector2 origin, Vector2 end) {
            var line = GetLine();
            line.Show();
            line.Draw(origin, end);
            _activeLines.Push(line);
            line.Color = _LineColor;
        }

        private RectLineRenderer GetLine() {
            if (_linesStorage.Count > 0) {
                return _linesStorage.Pop();
            }
            else {
                return CreateLine();
            }
        }

        private RectLineRenderer CreateLine() {
            var line = Instantiate(_line, _line.transform.parent);
            line.transform.SetSiblingIndex(_line.transform.GetSiblingIndex());
            return line;
        }
    }
}