﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

namespace Presentors {
    public class LetterItemView : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerUpHandler {
        [SerializeField] private Image _selector = null;
        [SerializeField] private TMP_Text _text = null;

        public event Action Selected;
        public event Action SelectingEnded;

        public Transform Pivot => transform.GetChild(0);
        public char Lettrer { get; private set; }

        public void Init(char letter) {
            Lettrer = letter;
            _text.text = letter.ToString();
        }

        public void Rotate(float zAngle) {
            transform.localRotation = Quaternion.Euler(0, 0, zAngle);
            Pivot.localRotation = Quaternion.Euler(0, 0, -zAngle);
        }

        public void SetSelected() => SetSelectorState(true);
        public void SetDeselected() => SetSelectorState(false);

        private void SetSelectorState(bool isVisible) => _selector.enabled = isVisible;

        public void OnPointerUp(PointerEventData eventData) {
            SelectingEnded?.Invoke();
        }

        public void OnPointerDown(PointerEventData eventData) {
            Selected?.Invoke();
        }

        public void OnPointerEnter(PointerEventData eventData) {
            Selected?.Invoke();
        }
    }
}