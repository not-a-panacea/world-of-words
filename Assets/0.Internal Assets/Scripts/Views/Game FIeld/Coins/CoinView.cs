﻿using Common.Types;
using Common.Views;
using UnityEngine;

namespace Views {
    public class CoinView : View {
        public Vector2IntSimple RelativePosition { get; set; }

        public void SetSize(Vector2 size) {
            var rectTr = transform as RectTransform;
            rectTr.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x);
            rectTr.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);
        }
    }
}
