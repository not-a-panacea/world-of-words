﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Views {
    public class GridCellItem : MonoBehaviour {
        [SerializeField] private Image hider = null;
        [SerializeField] private TMP_Text text = null;

        public void Init(char letter) {
            if (letter == default) {
                Hide();
            }
            else {
                hider.enabled = true;
                text.text = letter.ToString();
            }
        }

        public void ShowLetter() {
            hider.enabled = false;
        }

        private void Hide() {
            text.text = "";
            hider.enabled = false;
        }
    }
}