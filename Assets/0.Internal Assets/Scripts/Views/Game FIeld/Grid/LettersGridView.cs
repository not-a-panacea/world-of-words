using Common.Extensions;
using Common.Types;
using UnityEngine;
using UnityEngine.UI;

namespace Views {
    public class LettersGridView : MonoBehaviour {
        [SerializeField] private RectTransform _rectTransform = null;
        [SerializeField] private GridLayoutGroup _layoutGrid = null;
        [SerializeField, Range(0.0f, 1.0f)] private float _spacingPart = 0.1f;
        [SerializeField] private GridCellItem _gridCellItem = null;

        private GridCellItem[,] _celles;

        public Vector2 CellSize => _layoutGrid.cellSize;

        public void Init(char[,] grid) {
            var size = grid.GetSize();

            var rawCellWight = _rectTransform.rect.width / size.columns;
            var rawCellHeight = _rectTransform.rect.height / size.rows;
            var rawCellSide = Mathf.Min(rawCellWight, rawCellHeight);
            var spasing = rawCellSide * _spacingPart;
            var cellSide = rawCellSide - spasing;

            _layoutGrid.spacing = new Vector2(spasing, spasing);
            _layoutGrid.constraintCount = size.rows;
            var padding = ((rawCellWight - rawCellSide) * size.columns) + ((rawCellHeight - rawCellSide) * size.rows);
            padding /= 2;
            var isTopPadding = rawCellWight < rawCellHeight;
            if (isTopPadding) {
                _layoutGrid.padding = new RectOffset((int)(spasing / 2), 0, (int)padding, 0);
            }
            else {
                _layoutGrid.padding = new RectOffset((int)padding, 0, (int)(spasing / 2), 0);
            }
            _layoutGrid.cellSize = new Vector3(cellSide, cellSide);

            _celles = new GridCellItem[size.rows, size.columns];

            var parent = _gridCellItem.transform.parent;
            for (int i = 0; i < size.rows; i++) {
                for (int j = 0; j < size.columns; j++) {
                    var cell = Instantiate(_gridCellItem, parent);
                    cell.Init(grid[i, j]);
                    _celles[i, j] = cell;
                }
            }
            _gridCellItem.gameObject.SetActive(false); ;
        }

        public void ShowPositions(Vector2IntSimple[] positions) {
            foreach (var position in positions) {
                _celles[position.x, position.y].ShowLetter();
            }
        }

        public Vector3 GetCharPosition(Vector2IntSimple position) {
            return _celles[position.x, position.y].transform.position;
        }
    }
}