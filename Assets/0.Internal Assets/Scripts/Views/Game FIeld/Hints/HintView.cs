using Common.Views;
using Hints;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Views {
    public class HintView : View<(Hint hint, string countInfo)> {
        [SerializeField] private Button _button = null;
        [SerializeField] private Image _icon = null;
        [SerializeField] private TMP_Text _hintName = null;
        [SerializeField] private TMP_Text _hintCount = null;

        public event Action Clicked;

        protected override void OnInit() {
            _button.onClick.AddListener(OnButtonClicked);
        }

        protected override void OnAssign((Hint hint, string countInfo) data) {
            _icon.sprite = data.hint.Icon;
            _hintName.text = data.hint.Info;
            UpdateCount(data.countInfo);
        }

        public void UpdateCount(string countInfo) {
            _hintCount.text = countInfo;
        } 

        private void OnButtonClicked() {
            Clicked?.Invoke();
        }
    }
}
