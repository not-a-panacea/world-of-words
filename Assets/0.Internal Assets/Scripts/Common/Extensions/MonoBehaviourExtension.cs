using System;
using UnityEngine;

public static class MonoBehaviourExtension
{
    public static void Bind<T>(this GameObject gameObject, ref T variable) where T : Component {
        if (variable == false && gameObject.TryGetComponent<T>(out variable) == false) {
            variable = gameObject.GetComponentInChildren<T>();
            if (variable == false) {
                throw new NullReferenceException(typeof(T).ToString());
            }
        }
    }
}
