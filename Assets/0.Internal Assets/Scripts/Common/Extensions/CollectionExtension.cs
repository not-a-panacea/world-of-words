﻿using System;
using System.Collections.Generic;
using System.Collections;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Common.Extensions {
    public static class CollectionExtension {
        public static T GetRandomItem<T>(this IList<T> list) {
            return list[Random.Range(0, list.Count)];
        }

        public static int LoopIndex(this ICollection collection, int index) {
            return Mathf.Clamp(index, 0, collection.Count - 1);
        }

        public static T[] ToSingleElementArray<T>(this T element) {
            return new T[] { element };
        }

        public static List<T> ToSingleElementList<T>(this T element) {
            var list = new List<T>(1) { element };
            return list;
        }

        public static void Shuffle<T>(this T[] array) {
            for (int i = 0; i < array.Length; i++) {
                var randomIndex = Random.Range(0, array.Length);
                var temp = array[randomIndex];
                array[randomIndex] = array[i];
                array[i] = temp;
            }
        }

        public static (int rows, int columns) GetSize<T>(this T[,] array) {
            int rows = array.GetUpperBound(0) + 1;
            int columns = array.Length / rows;
            return (rows, columns);
        }

        public static V[,] Convert<V, K>(this K[,] array, Func<K, V> converter) {
            var size = array.GetSize();
            V[,] data = new V[size.rows, size.columns];
            for (int i = 0; i < size.rows; i++) {
                for (int j = 0; j < size.columns; j++) {
                    data[i, j] = converter(array[i, j]);
                }
            }
            return data;
        }

        public static T[,] CopyToDoubleRankArray<T>(this IList<T> iList, int rows, int columns) {
            var newArray = new T[rows, columns];
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    newArray[i, j] = iList[(i * columns) + j];
                }
            }
            return newArray;
        }

        public static T[] CopyToSigleRankArray<T>(this T[,] array) {
            var size = array.GetSize();
            var newArray = new T[size.rows * size.columns];
            for (int i = 0; i < size.rows; i++) {
                for (int j = 0; j < size.columns; j++) {
                    newArray[(i * size.columns) + j] = array[i,j];
                }
            }
            return newArray;
        }
    }
}