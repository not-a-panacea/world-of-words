using System.Collections.Generic;
using System.Linq;

namespace Common.Extensions {
    public static class TextExtension {

        public static bool IsVisible(this string str) {
            bool result = string.IsNullOrEmpty(str) == false 
                && string.IsNullOrWhiteSpace(str) == false
                && default(char).ToString() != str;
            return result;
        }

        public static class Converters {
            public static char FromString(string str) {
                if (str.IsVisible() == false) {
                    return default;
                }
                else {
                    return str[0];
                }
            }

            public static string FromChar(char ch) {
                return ch == default ? "" : ch.ToString();
            }
        }

        public static string[] GetUnicCharsWithRepeating(this IEnumerable<string> strings) {
            var unics = strings.GetUnicCharsAsStringArray();
            var repeating = new int[unics.Length];
            for (int i = 0; i < unics.Length; i++) {
                var ch = Converters.FromString(unics[i]);
                int repeatCount = 1;
                foreach (var word in strings) {
                    var r = word.Count(c => c.CompareTo(ch) == 0);
                    if (r > repeatCount) {
                        repeatCount = r;
                    }
                }
                repeating[i] = repeatCount;
                repeatCount = 1;
            }
            var sum = repeating.Sum();
            var repeatingUnics = new string[sum];
            var repeats = 0;
            for (int i = 0; i < unics.Length; i++) {
                var s = Enumerable.Repeat(unics[i], repeating[i]).ToArray();
                for (int j = 0; j < s.Length; j++) {
                    repeatingUnics[repeats + j] = s[j];
                }
                repeats += s.Length;
            }
            return repeatingUnics;
        }

        public static string[] GetUnicCharsAsStringArray(this IEnumerable<string> strings) {
            return strings.GetUnicChars().Select(Converters.FromChar).ToArray();
        }

        public static char[] GetUnicChars(this IEnumerable<string> strings) {
            return string.Concat(strings).Distinct().ToArray();
        }
    }
}