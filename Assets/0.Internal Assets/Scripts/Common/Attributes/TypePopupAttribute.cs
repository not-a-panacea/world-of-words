using UnityEngine;
using System;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Reflection;

namespace Common.Attributes {
    public class TypePopupAttribute : PropertyAttribute {
        public Type TargetType;

        public TypePopupAttribute() : this(default(Type)) { }

        public TypePopupAttribute(Type targetType) {
            TargetType = targetType;
        }
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(TypePopupAttribute))]
    public class TypePopupDrawer : PropertyDrawer {
        const int popupHeight = 18;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if (property.propertyType != SerializedPropertyType.ManagedReference) {
                EditorGUI.PropertyField(position, property, label, true);
                return;
            }

            var fieldType = GetFieldType(property);

            var availableTypes = Assembly.GetAssembly(fieldType).GetExportedTypes()
                .Where(t => fieldType.IsAssignableFrom(t) && !t.IsAbstract).ToArray();

            var showableNames = new string[availableTypes.Length + 1];
            showableNames[0] = "<none>";
            for (int i = 0; i < availableTypes.Length; i++) {
                showableNames[i + 1] = availableTypes[i].FullName;
            }

            var popupPos = new Rect(position) {
                height = popupHeight
            };

            var currentType = GetCurrentType(property);

            var selectedIndex = EditorGUI.Popup(popupPos, Array.IndexOf(availableTypes, currentType) + 1, showableNames);

            Type selectedType;
            if (selectedIndex > 0 && currentType != (selectedType = availableTypes[selectedIndex - 1])) {
                property.managedReferenceValue = Activator.CreateInstance(selectedType);
            } else if (selectedIndex == 0) property.managedReferenceValue = null;

            position.y += popupHeight;

            EditorGUI.PropertyField(position, property, label, true);
        }

        public static Type GetCurrentType(SerializedProperty property) {
            return GetType(property.managedReferenceFullTypename);
        }

        public static Type GetFieldType(SerializedProperty property) {
            return GetType(property.managedReferenceFieldTypename);
        }

        private static Type GetType(string typeName) {
            try {
                typeName = typeName.Substring(typeName.IndexOf(" ") + 1);
            } catch (Exception ex) {
                Debug.LogException(ex);
            }
            return Type.GetType(typeName);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            var additionValue = property.propertyType == SerializedPropertyType.ManagedReference ? popupHeight : 0;
            return EditorGUI.GetPropertyHeight(property, label) + additionValue;
        }
    }
#endif
}