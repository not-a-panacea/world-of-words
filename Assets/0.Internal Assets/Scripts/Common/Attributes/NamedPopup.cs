using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
#endif

namespace Common.Attributes {
    public class NamedPopup : PropertyAttribute {
        public bool AllowEmptyValue { get; private set; } = false;

        public string[] Names { get; protected set; }

        public NamedPopup(params string[] names) : this(false, names) { }
        public NamedPopup(IEnumerable<string> names, bool allowEmptyValue = false) : this(allowEmptyValue, names.ToArray()) { }
        public NamedPopup(bool allowEmptyValue, params string[] names) {
            SetNames(names);
            AllowEmptyValue = allowEmptyValue;
        }

        public NamedPopup(Type type, string methodName, bool allowEmpty = false) {
            var method = type.GetMethod(methodName);
            if (method != null) {
                object methodResult = method.Invoke(null, null);
                var names = methodResult as string[];
                if (names == null) {
                    if (methodResult is IEnumerable<string> strings) names = strings.ToArray();
                    if (methodResult is IEnumerable objs) {
                        var list = new List<string>();
                        foreach (object obj in objs) list.Add(obj?.ToString());
                        names = list.ToArray();
                    }
                }
                SetNames(names);
            } else {
                Debug.LogError("NO SUCH METHOD " + methodName + " FOR " + type);
            }
            AllowEmptyValue = allowEmpty;
        }

        public virtual void SetNames(string[] list) {
            Names = list;
        }
    }
}