using System;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Common.Attributes {
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(NamedPopup), true)]
    public class NamedPopupDrawer : PropertyDrawer {
        protected const int kDefaultHeight = 18;
        protected ushort additionHeight = 0;

        public virtual (string from, string to) ShouldReplace => (null, null);
        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            position.height = kDefaultHeight;
            var attr = attribute as NamedPopup;
            var names = attr.Names;
            // bool allowEmpty = attr.AllowEmptyValue;
            if (names == null || names.Length == 0) {
                GUI.Label(position, "empty names list");
                return;
            }

            var displayedNames = GetShowingList(names);
            // if (allowEmpty) displayedNames = displayedNames.Insert("none", 0);

            if (property.propertyType == SerializedPropertyType.String) {
                int index = Array.IndexOf(names, property.stringValue);
                // if (allowEmpty) index++; else 
                index = Mathf.Max(0, index);
                index = GetIndexFromPopup(position, property.displayName, index, displayedNames);
                // if (allowEmpty) index--;
                if (index < 0) {
                    additionHeight = kDefaultHeight;
                    var labelRect = new Rect(position);
                    position.y += additionHeight;
                    GUI.Label(labelRect, "INVALID NAME");
                    // } else if (allowEmpty && index == 0) {
                    //     additionHeight = 0;
                } else {
                    additionHeight = 0;
                    SetStringValue(property, names[index]);
                }
            } else if (property.propertyType == SerializedPropertyType.Integer) {
                property.intValue = EditorGUI.Popup(position, property.displayName, property.intValue, displayedNames);
            } else {
                base.OnGUI(position, property, label);
            }
        }

        public virtual int GetIndexFromPopup(Rect position, string label, int selectedIndex, string[] options) {
            return EditorGUI.Popup(position, label, selectedIndex, options);
        }

        public string[] GetShowingList(string[] values) {
            if (ShouldReplace.from != null && ShouldReplace.to != null) {
                return values.Select(item => item.Replace(ShouldReplace.from, ShouldReplace.to)).ToArray();
            }
            return values;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            return kDefaultHeight + additionHeight;
        }

        public virtual void SetStringValue(SerializedProperty property, string v) {
            property.stringValue = v;
        }
    }
#endif
}