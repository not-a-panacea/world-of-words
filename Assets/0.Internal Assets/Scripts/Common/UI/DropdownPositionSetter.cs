using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Common {
    [AddComponentMenu("UI/Custom/" + nameof(DropdownPositionSetter))]
    public class DropdownPositionSetter : MonoBehaviour, IPointerClickHandler {
        private ScrollRect _scrollRect;
        private float _lastPosition;

        public void OnPointerClick(PointerEventData eventData) {
            Observable.TimerFrame(2).Subscribe(_ => {
                EnsuareScrollRectExict();
                _scrollRect.verticalNormalizedPosition = _lastPosition;

                _scrollRect.ObserveEveryValueChanged(sr => sr.verticalNormalizedPosition)
                .TakeWhile(p => _scrollRect)
                .Subscribe(pos => _lastPosition = pos);
            });
        }

        private void EnsuareScrollRectExict() {
            if (_scrollRect == false) {
                var rects = GetComponentsInChildren<ScrollRect>();
                _scrollRect = rects.Last();
            }
        }
    }
}