﻿using UnityEngine;
using UnityEngine.UI;

namespace Common {
    [AddComponentMenu("UI/Custom/Progress View/Image Progress")]
    public class ImageProgressView : ProgressView {
        [SerializeField] private Image _image = null;

        protected override void OnMaxValueChanged(float delta) {
            SetFraction();
        }

        protected override void OnMinValueChanged(float delta) {
            SetFraction();
        }

        protected override void OnValueChanged(float delta) {
            SetFraction();
        }

        private void SetFraction() {
            _image.fillAmount = Fraction;
        }

        private void Reset() {
            Bind();
        }

        private void Bind() {
            gameObject.Bind(ref _image);
            _image.type = Image.Type.Filled;
        }
    }
}