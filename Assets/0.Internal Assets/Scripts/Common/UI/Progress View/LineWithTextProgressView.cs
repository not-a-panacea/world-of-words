﻿using UnityEngine;

namespace Common {
    [AddComponentMenu("UI/Custom/Progress View/Line With Text Progress")]
    public class LineWithTextProgressView : ProgressView {
        [SerializeField] private TextProgressView _textProgress = null;
        [SerializeField] private ProgressView _graphicProgress = null;

        protected override void OnMaxValueChanged(float delta) {
            _textProgress.MaxValue = MaxValue;
            _graphicProgress.MaxValue = MaxValue;

        }

        protected override void OnMinValueChanged(float delta) {
            _textProgress.MinValue = MinValue;
            _graphicProgress.MinValue = MinValue;

        }

        protected override void OnValueChanged(float delta) {
            _textProgress.Value = Value;
            _graphicProgress.Value = Value;
        }

        private void Reset() {
            Bind();
        }

        private void Bind() {
            gameObject.Bind(ref _textProgress);
            var views = GetComponentsInChildren<ProgressView>();
            foreach (var item in views) {
                if (item != _textProgress) {
                    _graphicProgress = item;
                    break;
                }
            }
        }
    }
}