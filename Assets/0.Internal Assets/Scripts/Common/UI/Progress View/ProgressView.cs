using Common.Types;
using UnityEngine;

namespace Common {
    public abstract class ProgressView : MonoBehaviour {
        public float MaxValue {
            get => _internalValue.Max; set {
                float delta = _internalValue.Max - value;
                _internalValue = FloatSpan.FromMinAndMax(value, _internalValue.Min);
                if (_internalValue.Contains(Value) == false) {
                    Value = _value;
                }
                OnMaxValueChanged(delta);
            }
        }
        public float MinValue { get => _internalValue.Min; set {
                float delta = _internalValue.Min - value;
                _internalValue = FloatSpan.FromMinAndMax(value, _internalValue.Max);
                if (_internalValue.Contains(Value) == false) {
                    Value = _value;
                }
                OnMinValueChanged(delta);
            }
        }
        public float Value { get => _value; set {
                float delta = _value - value;
                _value = Mathf.Clamp(value, _internalValue.Min, _internalValue.Max);
                OnValueChanged(delta);
            }
        }

        protected float Fraction => _internalValue.FractionOf(Value);

        private FloatSpan _internalValue;
        private float _value;

        protected abstract void OnMaxValueChanged(float delta);
        protected abstract void OnMinValueChanged(float delta);
        protected abstract void OnValueChanged(float delta);
    }
}