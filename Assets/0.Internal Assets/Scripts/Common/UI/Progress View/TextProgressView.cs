﻿using UnityEngine;
using TMPro;

namespace Common {
    [AddComponentMenu("UI/Custom/Progress View/Text Progress")]
    public class TextProgressView : ProgressView {
        [SerializeField] private TMP_Text _textField = null;
        [SerializeField] private string _template = "{0}/{1}";

        protected override void OnMaxValueChanged(float delta) {
            _textField.text = string.Format(_template, Value, MaxValue);
        }

        protected override void OnMinValueChanged(float delta) {
        }

        protected override void OnValueChanged(float delta) {
            _textField.text = string.Format(_template, Value, MaxValue);
        }

        private void Reset() {
            Bind();
        }

        private void Bind() {
            gameObject.Bind(ref _textField);
        }
    }
}