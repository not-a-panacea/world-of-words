﻿using UnityEngine;
using UnityEngine.UI;

namespace Common {
    [AddComponentMenu("UI/Custom/Progress View/Slider Progress")]
    public class SliderProgressView : ProgressView {
        [SerializeField] private Slider _slider = null;

        protected override void OnMaxValueChanged(float delta) {
            _slider.maxValue = MaxValue;
        }

        protected override void OnMinValueChanged(float delta) {
            _slider.minValue = MinValue;
        }

        protected override void OnValueChanged(float delta) {
            _slider.value = Value;
        }

        private void Bind() {
            gameObject.Bind(ref _slider);
        }

        private void Reset() {
            Bind();
        }
    }
}