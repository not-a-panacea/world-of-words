using UnityEngine;
using UnityEngine.UI;

namespace Common {
    [AddComponentMenu("UI/Custom/" + nameof(RectLineRenderer))]
    [RequireComponent(typeof(RectTransform), typeof(Canvas))]
    public class RectLineRenderer : MonoBehaviour {
        [SerializeField] private float _distanceMultiplyer = 100;
        [SerializeField] private Canvas _canvas = null;
        [SerializeField] private RectTransform _rectTransform = null;
        [SerializeField] private Image _image = null;

        private Vector2 _direction;
        private float _distance;
        private float _angle;
        private Vector3 _euler;
        private Quaternion _rotation;

        public Color Color { get => _image.color; set => _image.color = value; }

        private void Awake() {
            BindFields();
            _rectTransform.pivot = new Vector2(0, 0.5f);
        }

        private void Start() {
            var mainCanvas = GameObject.FindGameObjectWithTag(Constants.Tags.MainCanvas);
            if (mainCanvas) {
                _distanceMultiplyer = 1 / mainCanvas.transform.localScale.x;
            }
        }

        public void Show() => SetVisibility(true);
        public void Hide() => SetVisibility(false);

        private void SetVisibility(bool value) => _canvas.enabled = value;

        public void Draw(Vector2 origin, Vector2 end) {
            _distance = Vector2.Distance(origin, end);
            _direction = end - origin;
            _angle = Vector2.SignedAngle(Vector2.right, _direction);
            _euler.z = _angle;
            _rotation = Quaternion.Euler(_euler);
            _rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _distance * _distanceMultiplyer);
            _rectTransform.rotation = _rotation;
            _rectTransform.position = origin;
        }

        private void Reset() {
            BindFields();
        }

        private void BindFields() {
            gameObject.Bind<RectTransform>(ref _rectTransform);
            gameObject.Bind<Canvas>(ref _canvas);
            gameObject.Bind<Image>(ref _image);
        }
    }
}