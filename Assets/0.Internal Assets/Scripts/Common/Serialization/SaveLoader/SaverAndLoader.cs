﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Common.Serialization {
    public static class SaverAndLoader {
        #region PlayerPrefs
        public static bool HasData(string dataKey) {
            return PlayerPrefs.HasKey(dataKey);
        }

        public static void RemoveAll() {
            PlayerPrefs.DeleteAll();
        }

        public static void RemoveData(string dataKey) {
            PlayerPrefs.DeleteKey(dataKey);
        }

        public static void Save<T>(string dataKey, T dataToSave) {
            var data = JsonSerializer.Serialize(dataToSave);
            Save(dataKey, data);
        }

        public static void Save(string dataKey, string dataToSave) {
            PlayerPrefs.SetString(dataKey, dataToSave);
        }

        public static void Save(string dataKey, int dataToSave) {
            PlayerPrefs.SetInt(dataKey, dataToSave);
        }

        public static void Save(string dataKey, float dataToSave) {
            PlayerPrefs.SetFloat(dataKey, dataToSave);
        }

        public static T Load<T>(string dataKey) {
            var data = Load(dataKey, string.Empty);
            if (string.IsNullOrEmpty(data))
                return default;

            var deserializedObject = JsonSerializer.Deserialize<T>(data);
            return deserializedObject;
        }

        public static string Load(string dataKey, string defaultValue) {
            return PlayerPrefs.GetString(dataKey, defaultValue);
        }

        public static int Load(string dataKey, int defaultValue) {
            return PlayerPrefs.GetInt(dataKey, defaultValue);
        }

        public static float Load(string dataKey, float defaultValue) {
            return PlayerPrefs.GetFloat(dataKey, defaultValue);
        }
        #endregion

        #region Resources
        public static void SaveToResources(string fileName, string dataToSave) {
            var fullPath = string.Format("{0}/Resources/{1}.json", Application.dataPath, fileName);
            if (!File.Exists(fullPath)) {
                var steam = File.Create(fullPath);
                steam.Dispose();
                steam.Close();
            }
            File.WriteAllText(fullPath, dataToSave);
        }

        public static string LoadFromResources(string fileName) {
            var textAssets = Resources.Load<TextAsset>(fileName);
            if (textAssets != null)
                return textAssets.text;
            return string.Empty;
        }

        public static IDictionary<string, string> LoadAllFromResources(string folderName) {
            var textAssets = Resources.LoadAll<TextAsset>(folderName);
            return textAssets.ToDictionary(n => n.name, a => a.text);
        }
        #endregion

        #region InternalStorage
        public static void SaveToInternalStorage(string fileName, string dataToSave) {
            var fullPath = Path.Combine(Application.persistentDataPath, fileName);

            if (!File.Exists(fullPath)) {
                var steam = File.Create(fullPath);
                steam.Dispose();
                steam.Close();
            }
            File.WriteAllText(fullPath, dataToSave);
        }

        public static string LoadFromInternalStorage(string fileName) {
            var fullPath = Path.Combine(Application.persistentDataPath, fileName);
            return File.ReadAllText(fullPath);
        }

        public static IDictionary<string, string> LoadAllFromInternalStorage() {
            var output = new Dictionary<string, string>();
            var fullPath = Application.persistentDataPath;
            if (!Directory.Exists(fullPath))
                return output;

            var files = Directory.GetFiles(fullPath);
            for (var i = 0; i < files.Length; i++) {
                try {
                    output.Add(Path.GetFileName(files[i]), File.ReadAllText(files[i]));
                }
                catch (Exception e) {
                    Debug.LogError(e.Message);
                }
            }
            return output;
        }
        #endregion

        #region Other
        public static string LoadFromData(string fileName) {
            var readPath = Application.dataPath + "/Data/" + fileName + ".json";
            var source = File.ReadAllText(readPath);
            if (!string.IsNullOrEmpty(source))
                return source;
            return string.Empty;
        }
        #endregion

#if UNITY_EDITOR
        [UnityEditor.MenuItem("Tools/Delete Player Prefs")]
        private static void DeletePrefs() {
            PlayerPrefs.DeleteAll();
        }
#endif
    }
}
