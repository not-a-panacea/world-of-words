using System.Text;

namespace Common.Serialization {
    public class BinarySerializer : ISerializer {
        public Encoding Encoding { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        public T DeserializeFromBinary<T>(byte[] binaryData) {
            throw new System.NotImplementedException();
        }

        public T DeserializeFromJson<T>(string jsonData) {
            throw new System.NotImplementedException();
        }

        public byte[] SerializeToBinary<T>(T data) {
            throw new System.NotImplementedException();
        }

        public string SerializeToJson<T>(T data) {
            throw new System.NotImplementedException();
        }
    }
}