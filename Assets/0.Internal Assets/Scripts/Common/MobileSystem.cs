using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Common {
    public static class MobileSystem {
        public static event Action SavingData;

        static MobileSystem() {
#if UNITY_ANDROID || UNITY_IOS
            Observable.EveryApplicationFocus().Subscribe(focus => { if (focus == false) SavingData?.Invoke(); });
            Observable.EveryApplicationPause().Subscribe(pause => { if (pause == true) SavingData?.Invoke(); });
            Observable.OnceApplicationQuit().Subscribe(_ => { SavingData?.Invoke(); });
#elif UNITY_EDITOR
            var trigger = new GameObject("Application Lifetime Object").AddComponent<ObservableDestroyTrigger>();
            trigger.OnDestroyAsObservable().Subscribe(_ => { SavingData?.Invoke(); });
#endif
        }
    }
}