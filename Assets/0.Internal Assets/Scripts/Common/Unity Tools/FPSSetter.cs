using UnityEngine;
using UnityEngine.SceneManagement;
using System;

namespace UnityLibrary.Debugging
{
    public sealed class FPSSetter : MonoBehaviour
    {
        public static FPSSetter Instance;

        [SerializeField, Range(0, 144)] private int targetFrameRate = 60;
        [SerializeField] private VSyncType vSync = VSyncType.DontSync;

        public int TargetFrameRate { 
            get => targetFrameRate; 
            set { targetFrameRate = value; 
                Application.targetFrameRate = targetFrameRate; } }

        private void Awake()
        {
            Instance = this;
            Apply();
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        public static void Apply() {
            if (Instance) {
                if (Instance.vSync == VSyncType.DontSync) {
                    Application.targetFrameRate = Instance.targetFrameRate;
                    Time.fixedDeltaTime = 1.0f / Mathf.Clamp(Instance.targetFrameRate, 10.0f, 60.0f);
                }
                else {
                    QualitySettings.vSyncCount = (int)Instance.vSync;
                }
            }
        }

        private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1) {
            Apply();
        }

        private void OnValidate() {
            Apply();
        }

        [Serializable]
        public enum VSyncType {
            DontSync = 0,
            SyncEveryVBlank = 1,
            SyncEverySecondVBlanc = 2
        }
    }
}
