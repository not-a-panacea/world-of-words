﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Views {
    public abstract class View : DisposableBehaviour, IView, IDisposable {
        private IShowHideHandler showHideHandler;
        private bool initialized = false;

        private IShowHideHandler ShowHideHandler {
            get {
                EnsureInitialized();
                return showHideHandler;
            }
        }

        private void Awake() {
            EnsureInitialized();
        }

        private void EnsureInitialized() {
            if (!initialized) {
                initialized = true;
                InitAndSubscribeHandler();
                OnInit();
            }
        }

        private void InitAndSubscribeHandler() {
            if (this is IShowHideHandler thisHandler) {
                showHideHandler = thisHandler;
            } else if (showHideHandler == null && !TryGetComponent(out showHideHandler)) {
                showHideHandler = CreateDefault(gameObject);
            }

            if (showHideHandler is AnimatedShowHideHandler animated) {
                animated.ShowStarted += OnBeforeShow;
                animated.HideStarted += OnBeforeHide;
            } else {
                showHideHandler.Shown += OnBeforeShow;
                showHideHandler.Hided += OnBeforeHide;
            }

            showHideHandler.Shown += OnShow;
            showHideHandler.Hided += OnHide;
        }

        public event Action Showed {
            add {
                showHideHandler.Shown += value;
            }
            remove {
                showHideHandler.Shown += value;
            }
        }

        public event Action Hided {
            add {
                showHideHandler.Hided += value;
            }
            remove {
                showHideHandler.Hided -= value;
            }
        }

        public void Show() {
            showHideHandler.Show();
        }

        public void Hide() {
            showHideHandler.Hide();
        }

        // default staff such as subscriptions and default state features
        protected virtual void OnInit() { }
        protected virtual void OnShow() { }
        protected virtual void OnHide() { }
        protected virtual void OnBeforeShow() { }
        protected virtual void OnBeforeHide() { }

        protected static ShowHideHandler CreateDefault(GameObject target) {
            if (target.TryGetComponent<CanvasGroup>(out _)) {
                return target.AddComponent<CanvasGroupShowHideHandler>();
            }
            else if (target.TryGetComponent<Canvas>(out _)) {
                return target.AddComponent<CanvasShowHideHandler>();
            } else {
                return target.AddComponent<ActivationShowHideHandler>();
            }
        }
    }

    public abstract class View<TData> : View, IView<TData> {
        public TData Data { get; private set; }

        public void Show(TData data) {
            Assign(data);
            Show();
        }

        public void Assign(TData data) {
            Data = data;
            OnAssign(data);
        }

        protected abstract void OnAssign(TData data);

        public static List<TView> CreateCollection<TView, KData>(IEnumerable<KData> data, TView prefab, Transform parent, Action<TView> initializator) where TView : View<KData> {
            List<TView> result = new List<TView>();
            foreach (var item in data) {
                var view = Instantiate(prefab, parent);
                view.Assign(item);
                result.Add(view);
                initializator(view);
            }
            return result;
        }
    }
}