﻿using System;

namespace Common.Views {
    public interface IShowHideHandler : IDisposable {
        event Action Shown;
        event Action Hided;

        ShowState State { get; }

        void Show();
        void Hide();
    }

    public abstract class ShowHideHandler : DisposableBehaviour, IShowHideHandler {
        protected const string menuPath = "UI/ShowHide Handler/";

        public abstract ShowState State { get; }

        public abstract event Action Shown;
        public abstract event Action Hided;

        public abstract void Show();
        public abstract void Hide();

        protected virtual void Reset() { }
    }

    public abstract class AnimatedShowHideHandler : ShowHideHandler {
        // следующие два ивента под вопросом. вероятно, сообразнее вынести их в отдельный интерфейс-наследник,
        // например, - IAnimatedShowHideHandler
        public abstract event Action ShowStarted;
        public abstract event Action HideStarted;

        public abstract bool IsAnimating { get; }

        public abstract void InstantShow();
        public abstract void InstantHide();
    }
}