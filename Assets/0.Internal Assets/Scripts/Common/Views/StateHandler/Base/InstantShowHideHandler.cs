﻿using System;

namespace Common.Views {
    public abstract class InstantShowHideHandler : ShowHideHandler {
        private bool initialized;

        public override event Action Shown;
        public override event Action Hided;

        public override ShowState State => ShowState.Create(IsVisibleInternal());

        protected void Awake() {
            EnsureInitialized();
        }

        protected void EnsureInitialized() {
            if (!initialized) {
                initialized = true;
                OnInit();
                var state = State;
                SetVisibility(state.IsShowing);
                InvokeVisibilityEvent(state.IsShowing);
            }
        }

        protected virtual void OnInit() { }

        public override void Show() {
            TrySetState(isVisible: true);
        }

        public override void Hide() {
            TrySetState(isVisible: false);
        }

        protected void InvokeVisibilityEvent(bool isVisible) {
            if (isVisible) {
                Shown?.Invoke();
            } else {
                Hided?.Invoke();
            }
        }

        protected bool TrySetState(bool isVisible) {
            EnsureInitialized();
            if (IsVisibleInternal() == isVisible) return false;
            SetVisibility(isVisible);
            InvokeVisibilityEvent(isVisible);
            return true;
        }

        protected abstract bool IsVisibleInternal();
        protected abstract void SetVisibility(bool value);
    }
}