namespace Common.Views {
    public struct ShowState {
        const byte SHOW_MASK = 0b_0001;
        const byte TRANSITION_MASK = 0b_0010;

        private byte data;

        public static readonly ShowState Hiding = new ShowState(0);
        public static readonly ShowState Showing = new ShowState(SHOW_MASK);
        public static readonly ShowState ShowingAnimate = new ShowState(SHOW_MASK | TRANSITION_MASK);
        public static readonly ShowState HidingAnimate = new ShowState(TRANSITION_MASK);

        public static ShowState Create(bool isShowing) {
            return isShowing ? Showing : Hiding;
        }

        public ShowState(byte data) {
            this.data = data;
        }

        public ShowState SwitchVisibility() {
            data ^= SHOW_MASK;
            return this;
        }

        public ShowState SwitchTransitionStatus() {
            data ^= TRANSITION_MASK;
            return this;
        }

        public bool IsShowing => this != Hiding;
        public bool IsTransiting => (data & TRANSITION_MASK) > 0;

        #region OVERRIDINGS
        public override bool Equals(object obj) {
            return obj is ShowState state && this == state;
        }

        public override int GetHashCode() {
            return data.GetHashCode();
        }

        public override string ToString() {
            return data.ToString();
        }

        #region STATIC OPERATORS
        public static implicit operator ShowState(byte data) {
            return new ShowState(data);
        }

        public static bool operator >(ShowState s1, ShowState s2) {
            return s1.data > s2.data;
        }

        public static bool operator <(ShowState s1, ShowState s2) {
            return s1.data < s2.data;
        }

        public static bool operator >=(ShowState s1, ShowState s2) {
            return s1.data >= s2.data;
        }

        public static bool operator <=(ShowState s1, ShowState s2) {
            return s1.data <= s2.data;
        }

        public static bool operator ==(ShowState s1, ShowState s2) {
            return s1.data == s2.data;
        }

        public static bool operator !=(ShowState s1, ShowState s2) {
            return !(s1 == s2);
        }

        public static ShowState operator |(ShowState s1, ShowState s2) {
            return (byte)(s1.data | s2.data);
        }

        public static ShowState operator &(ShowState s1, ShowState s2) {
            return (byte)(s1.data & s2.data);
        }

        public static ShowState operator ^(ShowState s1, ShowState s2) {
            return (byte)(s1.data ^ s2.data);
        }

        public static ShowState operator +(ShowState s1, byte b) {
            return (byte)(s1.data + b);
        }

        public static ShowState operator -(ShowState s1, byte b) {
            return (byte)(s1.data - b);
        }

        public static ShowState operator ++(ShowState s1) {
            return (byte)(s1.data + 1);
        }

        public static ShowState operator --(ShowState s1) {
            return (byte)(s1.data - 1);
        }
        #endregion STATIC OPERATORS
        #endregion OVERRIDINGS
    }
}