﻿using UnityEngine;

namespace Common.Views {
    [RequireComponent(typeof(CanvasGroup))]
    [AddComponentMenu(menuPath + "Canvas Group Handler")]
    public class CanvasGroupShowHideHandler : InstantShowHideHandler {
        [SerializeField, Range(.01f, .99f)] private float alphaShowThreshold = .5f;

        [SerializeField, HideInInspector] private CanvasGroup canvasGroup = null;

        protected override void Reset() {
            if (!canvasGroup) canvasGroup = GetComponent<CanvasGroup>();
        }

        protected override void OnInit() {
            if (!canvasGroup) canvasGroup = GetComponent<CanvasGroup>();
        }

        protected override bool IsVisibleInternal() {
            return canvasGroup.alpha > alphaShowThreshold;
        }

        protected override void SetVisibility(bool isVisible) {
            canvasGroup.alpha = isVisible ? 1 : 0;
            canvasGroup.blocksRaycasts = isVisible;
            canvasGroup.interactable = isVisible;
        }
    }
}