﻿using UnityEngine;

namespace Common.Views {
    [RequireComponent(typeof(Canvas))]
    [AddComponentMenu(menuPath + "Canvas Handler")]
    public class CanvasShowHideHandler : InstantShowHideHandler {
        [SerializeField, HideInInspector] private Canvas canvas = null;

        protected override void OnInit() {
            if (!canvas) canvas = GetComponent<Canvas>();
        }

        protected override void Reset() {
            if (!canvas) canvas = GetComponent<Canvas>();
        }

        protected override bool IsVisibleInternal() {
            return canvas.enabled;
        }

        protected override void SetVisibility(bool isVisible) {
            canvas.enabled = isVisible;
        }
    }
}