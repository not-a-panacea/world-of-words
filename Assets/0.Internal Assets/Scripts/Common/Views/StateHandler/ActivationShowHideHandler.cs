﻿using UnityEngine;

namespace Common.Views
{
    [AddComponentMenu(menuPath + "Game Object Active Handler")]
    public class ActivationShowHideHandler : InstantShowHideHandler {
        protected override bool IsVisibleInternal() {
            return gameObject.activeSelf;
        }

        protected override void SetVisibility(bool value) {
            gameObject.SetActive(value);
        }
    }
}