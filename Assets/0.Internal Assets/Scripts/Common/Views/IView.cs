﻿using System;

namespace Common.Views {
    public interface IView {
        event Action Showed;
        event Action Hided;

        void Show();
        void Hide();
    }

    public interface IView<TData> : IView {
        TData Data { get; }
        void Assign(TData data);
    }
}
