using UnityEngine;

namespace Common.Types {
    [System.Serializable]
    public struct FloatSpan {
        [SerializeField] private float since;
        [SerializeField, Min(0)] private float duration;

        public float Duration => duration;
        public float Min => since;
        public float Max => since + duration;

        public bool Contains(float value) {
            return value >= since && value <= Max;
        }

        public float FractionOf(float value) {
            return (value - since) / duration;
        }

        public float ValueOfFraction(float fraction) {
            return since + fraction * duration;
        }

        public float RollRandom() {
            return duration == 0 ? since : Random.Range(since, Max);
        }

        public float Clamp(float value) => Mathf.Clamp(value, Min, Max);

        public static FloatSpan FromMinAndMax(float a, float b) {
            float since;
            float duration;
            var diff = a - b;
            if (diff < 0) {
                since = a;
                duration = -diff;
            } else {
                since = b;
                duration = diff;
            }
            return new FloatSpan() {
                since = since,
                duration = duration
            };
        }

        public static implicit operator FloatSpan(IntSpan intSpan) {
            return FromMinAndMax(intSpan.Min, intSpan.Max);
        }
        public static implicit operator FloatSpan((float, float) pair) {
            return FromMinAndMax(pair.Item1, pair.Item2);
        }
    }
}
