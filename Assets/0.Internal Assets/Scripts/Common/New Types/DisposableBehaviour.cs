using System;
using UnityEngine;
using UniRx;

public class DisposableBehaviour : MonoBehaviour, IDisposable {
    protected readonly CompositeDisposable subsriptions = new CompositeDisposable();

    public void Dispose() {
        OnDisposing();
        subsriptions.Dispose();
        OnDisposed();
    }

    private void OnDestroy() {
        Dispose();
    }

    protected virtual void OnDisposing() { }
    protected virtual void OnDisposed() { }

    public void AddToThis(IDisposable disposable) {
        subsriptions.Add(disposable);
    }
}

public static class DisposableBehaviourExtensions {
    public static T AddTo<T>(this T disposable, DisposableBehaviour db) where T : IDisposable {
        db.AddToThis(disposable);
        return disposable;
    }

    public static T AddToScene<T>(this T disposable) where T : IDisposable {
        return disposable.AddTo(SceneLifetimeObject.Current);
    }
}
