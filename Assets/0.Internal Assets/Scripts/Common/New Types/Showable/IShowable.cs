using UnityEngine;

public interface IShowable
{
    string Info { get; }
    Sprite Icon { get; }
}

public interface INameShowable :IShowable {
    string Name { get; }
}

public class Showable : IShowable {
    public string Info => _info;
    public Sprite Icon => _icon;

    private string _info;
    private Sprite _icon;

    public Showable(string info, Sprite icon) {
        _info = info;
        _icon = icon;
    }
}
