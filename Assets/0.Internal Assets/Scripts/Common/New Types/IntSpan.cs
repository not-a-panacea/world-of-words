using UnityEngine;
using System;
using Random = UnityEngine.Random;

namespace Common.Types {
    [Serializable]
    public struct IntSpan {
        [SerializeField] private int since;
        [SerializeField] private ushort duration;

        public int Min => since;
        public int Max => since + duration;

        private IntSpan(int since, ushort duration) {
            this.since = since;
            this.duration = duration;
        }

        public bool Contains(int value) {
            return value >= since && value <= Max;
        }

        public int RollRandom() {
            return Random.Range(since, since + duration + 1);
        }

        public static IntSpan Create(int v1, int v2) {
            var diff = v1 - v2;
            int since;
            ushort duration;
            if (diff < 0) {
                since = v1;
                duration = (ushort)-diff;
            } else {
                since = v2;
                duration = (ushort)diff;
            }
            return new IntSpan(since, duration);
        }

        public static implicit operator IntSpan((int, int) pair) {
            return Create(pair.Item1, pair.Item2);
        }
    }
}
