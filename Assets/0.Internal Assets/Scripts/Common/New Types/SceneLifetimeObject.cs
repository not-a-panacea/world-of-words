using System;
using UniRx;
using UnityEngine;

public class SceneLifetimeObject : DisposableBehaviour {
    public static event Action DestroyingAny;

    private static SceneLifetimeObject current = null;
    public static SceneLifetimeObject Current => current ? current : (current = CreateInstance());

    public event Action DestroyingThis;

    public IObservable<Unit> ObserveDestroying() {
        return Observable.FromEvent(h => DestroyingThis += h, h => DestroyingThis -= h);
    }

    private static SceneLifetimeObject CreateInstance() {
        var instance = new GameObject(nameof(SceneLifetimeObject)).AddComponent<SceneLifetimeObject>();
        return instance;
    }

    protected override void OnDisposing() {
        DestroyingThis?.Invoke();
        DestroyingAny?.Invoke();
    }
}
