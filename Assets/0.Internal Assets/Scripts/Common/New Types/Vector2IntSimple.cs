using System;
using UnityEngine;

namespace Common.Types {
    [Serializable]
    public struct Vector2IntSimple : IEquatable<Vector2IntSimple> {
        public int x;
        public int y;

        public Vector2IntSimple(Vector2Int vector) : this(vector.x, vector.y){

        }

        public Vector2IntSimple(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public bool Equals(Vector2IntSimple other) {
            return x == other.x && y == other.y;
        }

        public static implicit operator Vector2IntSimple(Vector2Int vector) {
            return new Vector2IntSimple(vector);
        }

        public static implicit operator Vector2Int(Vector2IntSimple vector) {
            return new Vector2Int(vector.x, vector.y);
        }
    }
}