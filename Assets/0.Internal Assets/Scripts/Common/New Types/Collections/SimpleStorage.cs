using FullSerializer;
using System;
using System.Collections.Generic;

namespace Common.Collections {
    [Serializable]
    public class SimpleStorage {
        [fsProperty] private Dictionary<string, int> _collection;

        public SimpleStorage() {
            _collection = new Dictionary<string, int>();
        }

        public IEnumerable<(string name, int count)> GetContent() {
            foreach (var pair in _collection) {
                yield return (pair.Key, pair.Value);
            }
        }

        public void Add(string itemName, int count = 1) {
            int currentCount = GetCount(itemName);
            if (count < 0) {
                throw new ArgumentException($"Wrong count: {count}");
            }
            currentCount += count;
            _collection[itemName] = currentCount;
        }

        public int GetCount(string itemName) {
            int count = 0;
            _collection.TryGetValue(itemName, out count);
            return count;
        }

        public void Substract(string itemName, int count = 1) {
            int currentCount = GetCount(itemName);
            if (currentCount < count || count < 0) {
                throw new ArgumentException($"Wrong count: {count}");
            }
            currentCount -= count;
            _collection[itemName] = currentCount;
        }
    }
}