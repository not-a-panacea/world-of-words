using System;

namespace Common.Data {
    public abstract class DataContainer<TData> {
        public event Action ValueChanged;

        public TData Data {
            get => GetData(); set {
                SetData(value);
                ValueChanged?.Invoke();
            }
        }

        protected abstract TData GetData();
        protected abstract void SetData(TData data);
    }
}
