﻿using Common.Serialization;
using System;
using System.IO;
using UnityEngine;

public static class SaveLoader {
    #region Save
    public static void SaveJsonToInternalStorage<T>(T data) {
        SaveJsonToInternalStorage<T>(data, typeof(T).Name);
    }

    public static void SaveJsonToInternalStorage<T>(T data, string fileName) {
        SaveJsonToInternalStorage<T>("", fileName, data);
    }

    public static void SaveJsonToInternalStorage<T>(string path, T data) {
        SaveJsonToInternalStorage<T>(path, typeof(T).Name, data);
    }

    public static void SaveJsonToInternalStorage<T>(string path, string fileName, T data) {
        var fullPath = Constants.Paths.InternalStorage + path;
        fileName = fileName + Constants.FileExtensions.Json;
        SaveData(fullPath, fileName, data);
    }

    public static void SaveJsonToResources<T>(string path, string fileName, T data) {
        var fullPath = Constants.Paths.ResourcesFull + path;
        fileName = fileName + Constants.FileExtensions.Json;
        SaveData(fullPath, fileName, data);

#if UNITY_EDITOR
        UnityEditor.AssetDatabase.SaveAssets();
        UnityEditor.AssetDatabase.Refresh();
#endif
    }

    public static void SaveData<T>(string path, string fileName, T data) {
        EnsuareSerializationInitialized();

        var dataToSave = Serialization.SerializeToJson(data);
        if (Directory.Exists(path) == false) {
            Directory.CreateDirectory(path);
        }

        var fullPath = Path.Combine(path, fileName);
        File.WriteAllText(fullPath, dataToSave, Serialization.Encoding);
    }
    #endregion

    #region Load
    public static T LoadJsonToInternalStorage<T>(string path) {
        var fullPath = Constants.Paths.InternalStorage + path + Constants.FileExtensions.Json;
        return LoadData<T>(fullPath);
    }

    public static T LoadJsonFromResources<T>(string path) {
        var fullPath = Constants.Paths.ResourcesFull + path + Constants.FileExtensions.Json;
        return LoadData<T>(fullPath);
    }

    public static T LoadData<T>(string path) {
        EnsuareSerializationInitialized();

        if (File.Exists(path) == false) {
            return default;
        }
        else {
            try {
                var textData = File.ReadAllText(path);
                return Serialization.DeserializeFromJson<T>(textData);
            }
            catch (Exception ex) {
                Debug.LogException(ex);
            }

            return default;
        }
    }
    #endregion

    #region Initialization
    private static void EnsuareSerializationInitialized() {
        if (Serialization.IsInitialized == false) {
            Serialization.Init(SerializerType.FsSerializer);
        }
    }
    #endregion
}
