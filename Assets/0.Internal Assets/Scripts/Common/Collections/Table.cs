using Common.Extensions;
using FullSerializer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Collections {
    [Serializable]
    public class Table<T> {
        public (int rows, int colunms) Size { get => _size; }

        [fsProperty] private (int rows, int colunms) _size;
        [fsProperty] private readonly List<T> _data;

        public Table() : this(0, 0, Array.Empty<T>()) {
        }

        public Table((int rows, int colunms) size) : this(size.rows, size.colunms) {
        }

        public Table(int rows, int colunms) : this(rows, colunms, new T[rows * colunms]) {
        }

        public Table(T[,] array) : this(array.GetSize().rows, array.GetSize().columns, array.CopyToSigleRankArray()) {
        }

        public Table(int rows, int colunms, T[] source) {
            _size = (rows, colunms);
            _data = source.ToList();
        }

        public T this [int row, int colunm] {
            get {
                return _data[(row * _size.colunms) + colunm];
            }

            set {
                _data[(row * _size.colunms) + colunm] = value;
            }
        }

        public static implicit operator T[,](Table<T> table) {
            var array = table._data.CopyToDoubleRankArray(table.Size.rows, table.Size.colunms);
            return array;
        }

        public static Table<V> Convert<V, K>(Table<K> table, Func<K, V> converter) {
            var data = table._data.Select(e => converter(e)).ToArray();
            return new Table<V>(table.Size.rows, table.Size.colunms, data);
        }
    }
}