using DG.Tweening;
using System;
using UnityEngine;

namespace Common.SimpleAnimations {
    [Serializable]
    public class SimpleAnimationClip : AnimationClip {

        private Sequence _sequence;
        private Transform _target;

        private Vector3 _currentPosition;
        private Vector3 _currentRotation;
        private Vector3 _currentScale;

        public override Tween Play(Transform target) {
            if (target != null) {
                _target = target;
                _sequence = DOTween.Sequence(_target);
                return PlayInternal();
            }
            else {
                Debug.LogException(new NullReferenceException("Target is NULL"));
                return null;
            }
        }

        private Tween PlayInternal() {
            _currentPosition = _target.position;
            _currentRotation = _target.rotation.eulerAngles;
            _currentScale = _target.localScale;

            foreach (var key in _keys) {
                var sequence = DOTween.Sequence();
                var moveTween = CreateMoveTween(key);
                var rotTween = CreateRotationTween(key);
                var scaleTween = CreateScaleTween(key);
                _sequence.Append(Combine(sequence, moveTween, rotTween, scaleTween));
            }
            return _sequence;
        }

        private Sequence Combine(Sequence sequence, params Tween[] tweens) {
            foreach (var tween in tweens) {
                if (tween != null) {
                    sequence.Insert(0, tween);
                }
            }
            return sequence;
        }

        private Tween CreateMoveTween(AnimationKey key) {
            if (key.HasPositionOffset) {
                _currentPosition = _currentPosition + key.PositionOffset;
                Tween tween = _target.DOMove(_currentPosition, key.Duration);
                ApplyKeyData(tween, key);
                return tween;
            }
            else {
                return null;
            }
        }

        private Tween CreateRotationTween(AnimationKey key) {
            if (key.HasRotationOffset) {
                _currentRotation = _currentRotation + key.RotationOffset;
                Tween tween = _target.DORotate(_currentRotation, key.Duration);
                ApplyKeyData(tween, key);
                return tween;
            }
            else {
                return null;
            }
        }

        private Tween CreateScaleTween(AnimationKey key) {
            if (key.HasScaleOffset) {
                _currentScale = _currentScale + key.ScaleOffset;
                Tween tween = _target.DOScale(_currentScale, key.Duration);
                ApplyKeyData(tween, key);
                return tween;
            }
            else {
                return null;
            }
        }

        private void ApplyKeyData(Tween tween, AnimationKey key) {
            tween.SetEase(key.Ease);
            if (key.Loop) {
                tween.SetLoops(key.LoopsCount > 0 ? key.LoopsCount : -1);
            }
            if (key.IgnoreTimeScale) {
                tween.SetUpdate(true);
            }
        }
    }
}