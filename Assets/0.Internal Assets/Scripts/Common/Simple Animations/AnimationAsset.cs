using UnityEngine;

namespace Common.SimpleAnimations {
    [CreateAssetMenu(fileName = "Animation Asset", menuName = "Configs/Animation/Animation Asset")]
    public class AnimationAsset : ScriptableObject {
        [SerializeReference] private AnimationClip _clip = null;

        public AnimationClip Clip => _clip;
    }
}