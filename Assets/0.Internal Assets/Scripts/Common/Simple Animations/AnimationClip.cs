using DG.Tweening;
using System;
using UnityEngine;

namespace Common.SimpleAnimations {
    [Serializable]
    public abstract class AnimationClip {
        [SerializeField] protected AnimationKey[] _keys = null;

        public abstract Tween Play(Transform target);
    }
}