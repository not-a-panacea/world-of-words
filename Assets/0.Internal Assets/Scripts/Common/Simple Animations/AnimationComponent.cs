using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Common.SimpleAnimations {
    [AddComponentMenu("Animation/Animation Component")]
    public class AnimationComponent : MonoBehaviour {
        [SerializeReference] private AnimationClip _clip = null;
        [Header("Or")]
        [SerializeField] private AnimationAsset _asset = null;
        [Space(20)]
        [Header("Events")]
        [SerializeField] private UnityEvent _onClipStarted = null;
        [SerializeField] private UnityEvent _onClipEnded = null;

        private Tween _tween;

        public AnimationClip Clip { get => _clip; set => _clip = value; }

        private void Awake() {
            if (Clip == null && _asset != null && _asset.Clip != null) {
                Clip = _asset.Clip;
            }

            if (Clip == null) {
                LogExeption();
            }
        }

        [Button]
        public void Play() {
            Play(transform);
        }

        public void Stop() {
            if (Clip != null) {
                if (_tween != null) {
                    _tween.Kill();
                }
            }
            else {
                LogExeption();
            }
            
        }

        private void Play(Transform target) {
            if (Clip != null) {
                _tween.Kill();
                _tween = Clip.Play(target).OnComplete(() => _onClipEnded?.Invoke());
                _onClipStarted?.Invoke();
            }
            else {
                LogExeption();
            }
        }

        private void LogExeption() {
            Debug.LogException(new NullReferenceException($"GameObject: {name} has an empty animation clip"));
        }

        private void OnDisable() {
            Stop();
        }
    }
}