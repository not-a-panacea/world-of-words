using DG.Tweening;
using System;
using UnityEngine;

namespace Common.SimpleAnimations {
    [Serializable]
    public class AnimationKey {
        [Header("Time settings")]
        [SerializeField, Min(0)] private float _duration = 0.0f;
        [SerializeField] private bool _ignoreTimeScale = false;
        [SerializeField] private bool _loop = false;
        [SerializeField, Min(0)] private int _loopsCount = 0;
        [Header("Move settings")]
        [SerializeField] private Ease _ease = Ease.Linear;
        [Space]
        [SerializeField] private Vector3 _positionOffset = Vector3.zero;
        [SerializeField] private Vector3 _rotationOffset = Vector3.zero;
        [SerializeField] private Vector3 _scaleOffset = Vector3.zero;

        public float Duration { get => _duration; set => _duration = value; }
        public bool IgnoreTimeScale { get => _ignoreTimeScale; set => _ignoreTimeScale = value; }
        public bool Loop { get => _loop; set => _loop = value; }
        public int LoopsCount { get => _loopsCount; set => _loopsCount = value; }

        public Ease Ease { get => _ease; set => _ease = value; }

        public Vector3 PositionOffset { get => _positionOffset; set => _positionOffset = value; }
        public bool HasPositionOffset => PositionOffset != Vector3.zero;
        public Vector3 RotationOffset { get => _rotationOffset; set => _rotationOffset = value; }
        public bool HasRotationOffset => RotationOffset != Vector3.zero;
        public Vector3 ScaleOffset { get => _scaleOffset; set => _scaleOffset = value; }
        public bool HasScaleOffset => PositionOffset != Vector3.zero;
    }
}