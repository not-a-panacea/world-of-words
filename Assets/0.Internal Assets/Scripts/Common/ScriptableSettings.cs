using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Common {
    public abstract class ScriptableSettings : ScriptableObject {
        protected const string menuItemPrefix = "Game Settings/";
        protected const string pathTemplete = "Assets/" + Constants.Paths.Resources + menuItemPrefix + "{0}" + Constants.FileExtensions.Asset;

        public static T LoadOrCreate<T>() where T : ScriptableSettings {
            var type = typeof(T);
            return LoadOrCreate<T>(type.Name);
        }

        public static T LoadOrCreate<T>(string name) where T : ScriptableSettings {
            var settings = Resources.Load<T>(menuItemPrefix + name);
            if (!settings) {
                settings = CreateInstance<T>();
                settings.name = name;
#if UNITY_EDITOR
                AssetDatabase.CreateAsset(settings, string.Format(pathTemplete, name));
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
#endif
            }
            settings.OnLoad();
            Debug.Log($"loaded {name} asset");
            return settings;
        }

        protected static void SelectSettings<TSettings>(TSettings instance) where TSettings : ScriptableSettings {
#if UNITY_EDITOR
            if (!instance) return;
            Selection.activeObject = instance;
#endif
        }

        protected virtual void OnLoad() {          
        }
    }

    public abstract class ScriptableSettings<TSettings> : ScriptableSettings where TSettings : ScriptableSettings<TSettings> {
        private static TSettings current = null;
        public static TSettings Current => current ?? (current = LoadOrCreate());

        private static TSettings LoadOrCreate() {
            return LoadOrCreate<TSettings>();
        }

        protected static TSettings LoadOrCreate_DevSettings() {
            return LoadOrCreate<TSettings>("Dev/" + typeof(TSettings).Name);
        }

        protected static TSettings LoadOrCreate_ProSettings() {
            return LoadOrCreate<TSettings>("Pro/" + typeof(TSettings).Name);
        }

        protected static void OverrideCurrent(TSettings instance) {
            current = instance;
        }

        // #if UNITY_EDITOR
        //     [UnityEditor.MenuItem(menuItemPrefix + nameof())]
        //     private static void SelectSettingsAsset() {
        //         SelectSettings(Current);
        //     }
        // #endif
    }
}