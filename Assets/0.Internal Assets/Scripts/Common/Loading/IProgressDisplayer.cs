﻿namespace Common.Loading
{
    public interface IProgressDisplayer
    {
        float Value { get; }

        void Set(float value);
    }
}