﻿using Common.Views;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Loading
{
    [AddComponentMenu("Loadig/LoadingScreen")]
    public class LoadingScreen : View<AsyncOperation>
    {
        [SerializeField] private GameObject progressDispalyerObject = null;
        [SerializeField] private float progressLerpTime = 5;

        private IProgressDisplayer progressDiplayer;
        private List<AsyncOperation> targets = new List<AsyncOperation>();
        private float averageProgress = 0;
        private bool allTargetsIsDone = true;

        public bool AutoHide { get; set; } = true;

        protected override void OnInit()
        {
            progressDiplayer = progressDispalyerObject.GetComponent<IProgressDisplayer>();

            if (progressDiplayer == null)
            {
                Debug.LogError("Progress displayer is NULL!");
            }
        }

        protected override void OnAssign(AsyncOperation asyncOperation)
        {
            targets.Add(asyncOperation);
            allTargetsIsDone = false;

            StopAllCoroutines();
            StartCoroutine(AsyncProgress());
        }

        protected override void OnHide()
        {
            if (!allTargetsIsDone) {
                Show();
                return;
            }

            StopAllCoroutines();
            targets.Clear();
        }

        private IEnumerator AsyncProgress()
        {
            float prewProgress = 0;
            float lerpProgress = 0;
            float tempTime = 0;

            while (!allTargetsIsDone)
            {
                averageProgress = 0;
                allTargetsIsDone = true;
                foreach (var item in targets)
                {
                    averageProgress += item.progress;
                    if (!item.isDone)
                    {
                        allTargetsIsDone = false;
                    }
                }
                averageProgress = averageProgress / targets.Count;

                if (Mathf.Approximately(prewProgress, averageProgress) == false) {
                    prewProgress = averageProgress;
                    tempTime = 0;
                }
                lerpProgress = tempTime / progressLerpTime;

                progressDiplayer.Set(Mathf.Lerp(prewProgress, averageProgress, lerpProgress));

                yield return null;
            }

            yield return new WaitForSecondsRealtime(0.5f);
            if (AutoHide) Hide();
        }

        private void Reset() {
            var progress = GetComponentInChildren<IProgressDisplayer>();
            if (progress != null) {
                progressDispalyerObject = ((MonoBehaviour)progress).gameObject;
            }
        }
    }
}