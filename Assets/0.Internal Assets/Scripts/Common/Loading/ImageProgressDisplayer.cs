﻿using UnityEngine;
using UnityEngine.UI;

namespace Common.Loading
{
    [AddComponentMenu("Loadig/ImageProgressDisplayer")]
    [RequireComponent(typeof(Image))]
    public class ImageProgressDisplayer : MonoBehaviour, IProgressDisplayer
    {
        [SerializeField] private Image image;

        public float Value => image.fillAmount;

        private void Awake()
        {
            image = GetComponent<Image>();
        }

        public void Set(float value)
        {
            image.fillAmount = Mathf.Clamp01(value);
        }

        private void Reset() {
            image = GetComponent<Image>();
        }
    }
}