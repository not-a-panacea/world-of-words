﻿using UnityEngine;
using UnityEngine.UI;

namespace Common.Loading
{
    [AddComponentMenu("Loadig/SliderProgressDisplayer")]
    [RequireComponent(typeof(Slider))]
    public class SliderProgressDisplayer : MonoBehaviour, IProgressDisplayer
    {
        [SerializeField] private Slider slider;

        private void Awake()
        {
            slider = GetComponent<Slider>();
        }

        public float Value => slider.value;

        public void Set(float value)
        {
            slider.value = Mathf.Clamp01(value);
        }

        private void Reset() {
            slider = GetComponent<Slider>();
        }
    }
}