﻿using System.Threading.Tasks;
using Object = UnityEngine.Object;

public interface IAssetLoader<T> where T : Object {
    Task<T> LoadObject();
    void UnloadObject(Object obj);
}
