using System;
using Object = UnityEngine.Object;

public class AssetableObject<T> : LoadableObject where T : Object {
    public event Action Loaded;

    public T Asset { get; private set; }
    public bool IsLoaded => Asset != null;

    public async void Load(Action<T> onLoad) {
        if (IsLoaded == false) {
            var obj = await LoadObject();
            if (obj != null && obj is T asset) {
                Asset = asset;
                onLoad?.Invoke(Asset);
                Loaded?.Invoke();
            }
        }
    }

    protected override Object GetLoadedObject() {
        return Asset;
    }
}
