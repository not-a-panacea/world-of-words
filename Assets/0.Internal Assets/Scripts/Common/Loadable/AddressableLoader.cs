﻿using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using Object = UnityEngine.Object;

public class AddressableLoader : IAssetLoader<Object> {
    private AssetReference _reference;

    public AddressableLoader(AssetReference reference) {
        _reference = reference;
    }

    public async Task<Object> LoadObject() {
        var asyncOp = _reference.LoadAssetAsync<Object>();
        return await asyncOp.Task;
    }

    public void UnloadObject(Object obj) {
        if (obj != null) Addressables.Release<Object>(obj);
    }
}
