using System;
using System.Threading.Tasks;
using UnityEngine;
using Object = UnityEngine.Object;

public class InstanceableObject<T> : LoadableObject where T : Object {
    private T _prefab;

    public event Action InstanceCreated;

    public T Instance { get; private set; }
    public bool IsExist => Instance != null;

    public async void Instantiate(Action<T> onInstantiate = null) {
        await Task.Run(() => Instantiate(null, onInstantiate));
    }

    public async void Instantiate(Transform parent, Action<T> onInstantiate = null) {
        await Task.Run(() => Instantiate(parent, Vector3.zero, onInstantiate));
    }

    public async void Instantiate(Transform parent, Quaternion rotation, Action<T> onInstantiate = null) {
        await Task.Run(() => Instantiate(parent, Vector3.zero, rotation, onInstantiate));
    }

    public async void Instantiate(Transform parent, Vector3 position, Action<T> onInstantiate = null) {
        await Task.Run(() => Instantiate(parent, position, Quaternion.identity, onInstantiate));
    }

    public async void Instantiate(Transform parent, Vector3 position, Quaternion rotation, Action<T> onInstantiate = null) {
        if (_prefab == null) {
            var obj = await LoadObject();
            if (obj != null && obj is T asset) {
                _prefab = asset;
            }
        }

        if (IsExist == false) {
            Instance = Object.Instantiate(_prefab, position, rotation, parent);
            onInstantiate?.Invoke(Instance);
            InstanceCreated?.Invoke();
        }
    }

    protected override Object GetLoadedObject() {
        return _prefab;
    }
}
