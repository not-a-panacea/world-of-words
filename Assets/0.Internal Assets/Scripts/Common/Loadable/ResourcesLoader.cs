﻿using System.Threading.Tasks;
using UnityEngine;
using Object = UnityEngine.Object;

public class ResourcesLoader : IAssetLoader<Object> {
    private string _path;

    public ResourcesLoader(string path) {
        _path = path;
    }

    public async Task<Object> LoadObject() {
        return await Task.FromResult(Resources.Load(_path));
    }

    public void UnloadObject(Object obj) {
        if (obj != null) Resources.UnloadAsset(obj);
    }
}
