using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Object = UnityEngine.Object;

public abstract class LoadableObject : IDisposable {
    [SerializeField] private string _path = "";
    [SerializeField] private AssetReference _reference = null;

    private IAssetLoader<Object> _loader;

    protected async Task<Object> LoadObject() {
        if (_loader == null) {
            _loader = CreateLoader(this);
        }
        return await _loader.LoadObject();
    }

    protected abstract Object GetLoadedObject();

    private static IAssetLoader<Object> CreateLoader(LoadableObject loadable) {
        if (string.IsNullOrWhiteSpace(loadable._path) == false) {
            return new ResourcesLoader(loadable._path);
        }
        else if (loadable._reference != null)  {
            return new AddressableLoader(loadable._reference);
        }
        else {
            throw new ArgumentNullException();
        }
    }

    private void UnloadObject() {
        _loader.UnloadObject(GetLoadedObject());
    }

    public void Dispose() {
        UnloadObject();
    }
}
