﻿namespace Common.Streams {
    public static class StreamExtension {
        public static OtherStreamResult ToResult(this IStream stream) {
            return new OtherStreamResult(stream);
        }
    }
}