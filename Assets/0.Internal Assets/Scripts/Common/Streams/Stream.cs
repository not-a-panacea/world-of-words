using System;
using System.Collections.Generic;
using UniRx;

namespace Common.Streams {
    public class Stream<TWorkArgs> : IStream {
        private List<IStreamListener<TWorkArgs>> _listeners = new List<IStreamListener<TWorkArgs>>();
        private StreamListenerComparer<TWorkArgs> _comparer = new StreamListenerComparer<TWorkArgs>();
        private IStreamListener<TWorkArgs> _currentListener;
        private TWorkArgs _args;
        private IDisposable _listenersWaitor;
        private bool _started = false;

        public event Action Completed;

        public bool IsDisposed => _listeners == null;

        public void Start(TWorkArgs args, bool waitListeners = false) {
            if (_started) return;
            _args = args;
            if (waitListeners && _listeners.Count == 0) {
                _listenersWaitor = Observable.EveryUpdate()
                    .TakeWhile(_ => _listeners.Count == 0)
                    .Subscribe(x => {}, () => { Start(_args); });
                return;
            }
            _started = true;
            CheckCompletion();
            _currentListener?.StartWork(_args, default);
        }

        private void Continue(IListenerWorkResult result) {
            _currentListener.Completed -= Continue;
            if (result is OtherStreamResult streamResult) {
                (streamResult.Data as IStream).Completed += () => Continue(default);
                return;
            }
            CheckCompletion();
            _currentListener?.StartWork(_args, result);
        }

        private void CheckCompletion() {
            if (MoveNext() == false) {
                Completed?.Invoke();
                Dispose();
            }
        }
        
        private bool MoveNext() {
            bool result = _listeners.Count > 0;
            if (result) {
                _currentListener = _listeners[0];
                _currentListener.Completed += Continue;
                _listeners.Remove(_currentListener);
            }
            return result;
        }

        public void Subscribe(IStreamListener<TWorkArgs> listener) {
            _listeners.Add(listener);
            _listeners.Sort(_comparer);
        }

        public void Dispose() {
            _listeners.Clear();
            _listeners = null;
            _comparer = null;
            _currentListener = null;
            _args = default;
            _listenersWaitor?.Dispose();
            Completed = null;
        }
    }

    public interface IStream : IDisposable {
        event Action Completed;
    }

    public class StreamListenerComparer<TWorkArgs> : IComparer<IStreamListener<TWorkArgs>> {
        public int Compare(IStreamListener<TWorkArgs> x, IStreamListener<TWorkArgs> y) {
            return -x.Priority.CompareTo(y.Priority);
        }
    }

    public interface IStreamListener<TWorkArgs> {
        event Action<IListenerWorkResult> Completed;
        int Priority { get; }
        void StartWork(TWorkArgs args, IListenerWorkResult prewResult);
    }

    public interface IStreamListener<TWorkArgs, TWorkResult> : IStreamListener<TWorkArgs> {
        new event Action<IListenerWorkResult<TWorkResult>> Completed;

        void StartWork(TWorkArgs args, IListenerWorkResult<TWorkResult> prewResult);
    }

    public interface IListenerWorkResult {
        public object Data { get; }
    }

    public interface IListenerWorkResult<TData> : IListenerWorkResult {
        public new TData Data { get; }
    }

    public class OtherStreamResult : IListenerWorkResult {
        private IStream _stream;
        public object Data => _stream;

        public OtherStreamResult(IStream stream) {
            _stream = stream;
        }
    }
}