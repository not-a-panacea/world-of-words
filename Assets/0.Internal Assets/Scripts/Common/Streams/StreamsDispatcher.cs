﻿using System;
using System.Collections.Generic;

namespace Common.Streams {
    public static class StreamsDispatcher {
        private static Dictionary<Type, IStream> _streams = new Dictionary<Type, IStream>();

        public static void Subscribe<TWorkArgs>(IStreamListener<TWorkArgs> listener) {
            var stream = GetOrCreateStream<TWorkArgs>();
            stream.Subscribe(listener);
        }

        public static void Start<TWorkArgs>(TWorkArgs args, bool waitListeners = false) {
            var stream = GetOrCreateStream<TWorkArgs>();
            stream.Start(args, waitListeners);
        }

        public static void Stop<TWorkArgs>() {
            if (TryGetStream<TWorkArgs>(out var stream)) {
                stream.Dispose();
                RemoveStream<TWorkArgs>();
            }
        }

        public static void Clear() {
            _streams.Clear();
        }

        public static void Dispose() {
            foreach (var pair in _streams) {
                pair.Value.Dispose();
            }
            Clear();
        }

        private static bool TryGetStream<TWorkArgs>(out IStream stream) {
            var type = typeof(TWorkArgs);
            return _streams.TryGetValue(type, out stream);
        }

        private static Stream<TWorkArgs> GetOrCreateStream<TWorkArgs>() {
            var type = typeof(TWorkArgs);
            if (_streams.TryGetValue(type, out var stream)) {
                return (stream as Stream<TWorkArgs>);
            }
            else {
                var newStream = new Stream<TWorkArgs>();
                _streams.Add(type, newStream);
                newStream.Completed += () => RemoveStream<TWorkArgs>();
                return newStream;
            }
        }

        private static void RemoveStream<TWorkArgs>() {
            var type = typeof(TWorkArgs);
            _streams.Remove(type);
        }
    }
}