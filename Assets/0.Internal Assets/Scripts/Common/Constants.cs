using UnityEngine;

public static partial class Constants
{
    public static partial class Tags {
        public const string
            Unttaged = nameof(Unttaged),
            Respawn = nameof(Respawn),
            Finish = nameof(Finish),
            EditorOnly = nameof(EditorOnly),
            MainCamera = nameof(MainCamera),
            Player = nameof(Player),
            GameController = nameof(GameController),
            MainCanvas = nameof(MainCanvas);
    }

    public static partial class Scenes {
        public const string
            Loading = "Loading",
            MainMenu = "Main Menu",
            Game = "Game";
    }

    public static partial class Paths {
        public static string Assets => Application.dataPath + "/";
        public static string StreamingAssets => Application.streamingAssetsPath + "/";
        public static string InternalStorage => Application.persistentDataPath + "/";
        public static string ResourcesFull => $"{Application.dataPath}/{Resources}";

        public const string Resources = "Resources/";
    }

    public static partial class FileExtensions {
        public const string
            Meta = ".meta",
            Asset = ".asset",
            Unity = ".unity",
            Prefab = ".prefab",
            Json = ".json",
            Txt = ".txt",
            Bin = ".bin",
            CS = ".cs";
    }
}
