using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LevelEditor {
    public class LevelSettings : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _timer = null;
        [SerializeField] private TMP_InputField _steps = null;
        [SerializeField] private TMP_InputField _exp = null;
        [SerializeField] private Toggle _hasCoins = null;

        public LevelContainer Level {
            get => _level ?? (_level = new LevelContainer()); 
            set {
                _level = value;
                UpdateUI(_level);
            }
        }

        private LevelContainer _level;

        private void Awake() {
            Level.ValueChanged += () => UpdateUI(Level);

            _timer.onEndEdit.AddListener(SetTimer);
            _timer.onSubmit.AddListener(SetTimer);
            _timer.onDeselect.AddListener(SetTimer);

            _steps.onEndEdit.AddListener(SetSteps);
            _steps.onSubmit.AddListener(SetSteps);
            _steps.onDeselect.AddListener(SetSteps);

            _exp.onEndEdit.AddListener(SetExp);
            _exp.onSubmit.AddListener(SetExp);
            _exp.onDeselect.AddListener(SetExp);

            _hasCoins.onValueChanged.AddListener(b => Level.HasCoins = b);
        }

        private void SetTimer(string str) {
            if (int.TryParse(str, out var time)) {
                Level.Time = time;
            }
        }

        private void SetSteps(string str) {
            if (int.TryParse(str, out var steps)) {
                Level.Steps = steps;
            }
        }

        private void SetExp(string str) {
            if (int.TryParse(str, out var exp)) {
                Level.Exp = exp;
            }
        }

        private void UpdateUI(LevelContainer level) {
            _timer.SetTextWithoutNotify(level.Time.ToString());
            _steps.SetTextWithoutNotify(level.Steps.ToString());
            _exp.SetTextWithoutNotify(level.Exp.ToString());
            _hasCoins.SetIsOnWithoutNotify(level.HasCoins);
        }
    }
}
