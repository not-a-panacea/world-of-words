using Common.Extensions;
using Common.Serialization;
using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace LevelEditor {
    public class LevelEditorController : MonoBehaviour {
        [SerializeField] private FileMenu _fileMenu = null;
        [SerializeField] private CharsGridEditor _grid = null;
        [SerializeField] private CategorySettings _categorySettings = null;
        [SerializeField] private LevelSettings _levelSettings = null;
        [SerializeField] private LevelInfo _levelInfo = null;

        private List<Category> _categoriesData;
        private Category _defaultCategory;
        private Level _defaultLevel;
        private Crossword _defaultCrossword;

        private Dictionary<string, int> _existingLevels = new Dictionary<string, int>();
        private string _currentCategory;
        private int _currentLevel;

        private void Awake() {
            Application.targetFrameRate = 24;
            Serialization.Init(SerializerType.FsSerializer);
            InitDefaultData();

            if (GameDataLoader.HasData == false) {
                GameDataLoader.InitDataFolder();
                GameDataLoader.SaveCategoriesData(_categoriesData);
                _existingLevels.Add(_defaultCategory.Id, 1);
            }
            else {
                var path = GameDataLoader.FullLevelsPath;

                _categoriesData = GameDataLoader.LoadCategoriesData();
                if (_categoriesData == null) {
                    InitDefaultData();
                }

                var existingCategories = Directory.EnumerateDirectories(path).Select(s => s.Split('/').Last()).ToArray();

                ActualizeCategoryData(existingCategories);
                
                for (int i = 0; i < existingCategories.Length; i++) {
                    var categoryFolder = existingCategories[i];

                    var levels = GameDataLoader.LoadLevels(categoryFolder);

                    if (levels.Count == 0) {
                        CreateDefaultLevelFile(categoryFolder);
                        levels.Add(GameDataLoader.GetLevelName(0));
                    }

                    _existingLevels.Add(categoryFolder, levels.Count);
                }
            }

            _fileMenu.CategoryChanged += OnCategoryChanged;
            _fileMenu.LevelChanged += OnLevelChanged;
            _fileMenu.CreateClicked += OnCreateClicked;
            _fileMenu.SaveClicked += OnSaveClicked;
            _fileMenu.LoadClicked += OnLoadClicked;
            _fileMenu.ClearClicked += OnClearClicked;

            _levelInfo.Assign(_levelSettings.Level);
            _grid.DataChanged += UpdateCrosswordContainer;
        }

        private void Start() {
            var cateegoriesArray = _categoriesData.Select(c => c.Id).ToArray();
            _fileMenu.SetCategoriesList(cateegoriesArray);
            SetCurrentCategory(_defaultCategory.Id);
        }

        private void InitDefaultData() {
            _categoriesData = new List<Category>();
            _defaultCategory = new Category("Mix", 10, "������", OpeningType.Unavailable, 0);
            _categoriesData.Add(_defaultCategory);
            _defaultCrossword = new Crossword(Array.Empty<Crossword.WordData>(), CharsGridEditor.GetDefaultGrid().Convert(ch => ch.ToString()));
            _defaultLevel = new Level(_defaultCrossword);
        }

        private void CreateDefaultLevelFile(string categoryPath) {
            _currentLevel = 0;
            GameDataLoader.SaveLevel(categoryPath, _currentLevel, _defaultLevel);
        }

        private void ActualizeCategoryData(string[] categoryFolders) {
            for (int i = 0; i < categoryFolders.Length; i++) {
                var folder = categoryFolders[i];
                var category = _categoriesData.FirstOrDefault(c => c.Id == folder);
                if (category == null) {
                    _categoriesData.Add(new Category(folder, 0, folder, OpeningType.Unavailable, 0));
                }
            }

            List<Category> deleteBuffer = new List<Category>();
            for (int i = 0; i < _categoriesData.Count; i++) {
                var category = _categoriesData[i];

                if (Array.IndexOf(categoryFolders, category.Id) < 0) {
                    deleteBuffer.Add(category);
                }
            }

            foreach (var category in deleteBuffer) {
                _categoriesData.Remove(category);
            }
            deleteBuffer.Clear();
            _categoriesData.Sort();
            GameDataLoader.SaveCategoriesData(_categoriesData);
        }

        private int GetIndexOfCurrentCategory(out string[] allCategories) {
            allCategories = _categoriesData.Select(c => c.Id).ToArray();
            return Array.IndexOf(allCategories, _currentCategory);
        }

        private void SetCurrentCategory(string id) {
            UpdatePreviousCategory();

            _currentCategory = id;
            _categoriesData.Sort();
            var index = GetIndexOfCurrentCategory(out var categories);

            _fileMenu.SetCategoriesList(categories, index);
            _categorySettings.Category.Data = _categoriesData[index];
            SetLevelsForCurrentCategory();
        }

        private void UpdatePreviousCategory() {
            if (_currentCategory.IsVisible()) {
                var data = _categorySettings.Category.Data;
                var oldData = _categoriesData.FirstOrDefault(c => c.Id == data.Id);
                if (oldData != null) {
                    var oldIndex = _categoriesData.IndexOf(oldData);
                    _categoriesData[oldIndex] = data;
                }
                else {
                    Debug.LogError("WTF?!");
                }
            }
        }

        private void SetLevelsForCurrentCategory() {
            _fileMenu.SetLevelsList(Enumerable.Range(0, _existingLevels[_currentCategory]).ToArray());
            SetLevel(0);
        }

        private void SetLevel(int levelIndex) {
            _currentLevel = levelIndex;
            var level = GameDataLoader.LoadLevel(_currentCategory, _currentLevel);
            if (level == null) {
                level = _defaultLevel;
            }
            _levelSettings.Level.Data = level;
            _grid.Init(level.Crossword.Grid);
        }

        private void OnCategoryChanged(int categoryIndex) {
            if (categoryIndex >= _categoriesData.Count) {
                return;
            }

            var category = _categoriesData[categoryIndex];
            SetCurrentCategory(category.Id);
        }

        private void OnLevelChanged(int levelIndex) {
            if (levelIndex == _currentLevel) {
                return;
            }

            SetLevel(levelIndex);
        }

        private void OnCreateClicked() {
            var levelsCount = _existingLevels[_currentCategory];
            _currentLevel = levelsCount;
            levelsCount++;
            _existingLevels[_currentCategory] = levelsCount;

            GameDataLoader.SaveLevel(_currentCategory, _currentLevel, _defaultLevel);

            _fileMenu.SetLevelsList(Enumerable.Range(0, levelsCount).ToArray(), _currentLevel);
            _levelSettings.Level.Data = _defaultLevel;
            _grid.Init(_defaultLevel.Crossword.Grid);
        }

        private void UpdateCrosswordContainer() {
            var charArray = _grid.GetData().Convert(TextExtension.Converters.FromChar);
            var wordDataArray = _grid.GetWordDatas();
            _levelSettings.Level.Crossword.Data = new Crossword(wordDataArray, charArray);
        }

        private void OnSaveClicked() {
            UpdateCrosswordContainer();
            GameDataLoader.SaveLevel(_currentCategory, _currentLevel, _levelSettings.Level.Data);
        }

        private void OnLoadClicked() {
            SetLevel(_currentLevel);
        }

        private void OnClearClicked() {
            _levelSettings.Level.Data = _defaultLevel;
            _grid.Init(_defaultLevel.Crossword.Grid);
        }

        private void OnDestroy() {
            UpdatePreviousCategory();
            GameDataLoader.SaveCategoriesData(_categoriesData);
        }
    }
}
