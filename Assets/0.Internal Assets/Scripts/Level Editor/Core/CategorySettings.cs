using Model;
using System;
using System.Linq;
using TMPro;
using UnityEngine;

namespace LevelEditor {
    public class CategorySettings : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _name = null;
        [SerializeField] private TMP_Dropdown _openingType = null;
        [SerializeField] private TMP_InputField _cost = null;
        [SerializeField] private TMP_InputField _sortOrder = null;

        public CategoryContainer Category {
            get => _category ?? (_category = new CategoryContainer());
            set {
                _category = value;
                UpdateUI(_category);
            }
        }

        private CategoryContainer _category;
        OpeningType[] _openingTypes;

        private void Awake() {
            _openingTypes = (OpeningType[])Enum.GetValues(typeof(OpeningType));
            _openingType.ClearOptions();
            _openingType.AddOptions(_openingTypes.Select(t => t.ToString()).ToList());

            Category.ValueChanged += () => UpdateUI(Category);

            _name.onEndEdit.AddListener(str => Category.Name = str);
            _name.onSubmit.AddListener(str => Category.Name = str);
            _name.onDeselect.AddListener(str => Category.Name = str);

            _cost.onEndEdit.AddListener(SetPrice);
            _cost.onSubmit.AddListener(SetPrice);
            _cost.onDeselect.AddListener(SetPrice);

            _sortOrder.onEndEdit.AddListener(SetSortOrder);
            _sortOrder.onSubmit.AddListener(SetSortOrder);
            _sortOrder.onDeselect.AddListener(SetSortOrder);

            _openingType.onValueChanged.AddListener(index => {
                Category.OpeningType = _openingTypes[index];
            });
        }

        private void SetPrice(string str) {
            if (int.TryParse(str, out var price)) {
                Category.Price = price;
            }
        }

        private void SetSortOrder(string str) {
            if (int.TryParse(str, out var sortOrder)) {
                Category.SortOrder = sortOrder;
            }
        }

        private void UpdateUI(CategoryContainer category) {
            _name.SetTextWithoutNotify(category.Name);
            _openingType.SetValueWithoutNotify(Array.IndexOf(_openingTypes, category.OpeningType));
            _cost.SetTextWithoutNotify(category.Price.ToString());
            _sortOrder.SetTextWithoutNotify(category.SortOrder.ToString());
        }
    }
}
