using TMPro;
using UnityEngine;
using System.Linq;

namespace LevelEditor {
    public class LevelInfo : MonoBehaviour
    {
        [SerializeField] private TMP_Text _text = null;
        [SerializeField] private int _maxLettersCount = 6;
        [SerializeField] private Color _normalColor = Color.black;
        [SerializeField] private Color _warningColor = Color.black;
        [SerializeField] private Color _alertColor = Color.red;

        private LevelContainer _levelContainer;
        private Color[] _messageColors;

        public void Assign(LevelContainer levelContainer) {
            _levelContainer = levelContainer;
            _levelContainer.Crossword.ValueChanged += OnCrosswordChanged;
            _messageColors = new Color[3] { _normalColor, _warningColor, _alertColor };
        }

        private void OnCrosswordChanged() {
            var letters = _levelContainer.Crossword.Letters;
            var lettersCount = letters.Length;
            var wordsCount = _levelContainer.Crossword.Words.Length;
            var lettersStr = string.Join(", ", letters);
            var wordsStr = string.Join(", ", _levelContainer.Crossword.Words.Select(wd => wd.Word));
            _text.text = $"����: {lettersCount}/{_maxLettersCount}\n{lettersStr}\n\n����: {wordsCount} \n{wordsStr}";
            _text.color = GetColor(lettersCount);
        }

        private Color GetColor(int letersCount) {
            return _messageColors[Mathf.Clamp(letersCount - _maxLettersCount, 0, _messageColors.Length - 1)];
        }
    }
}
