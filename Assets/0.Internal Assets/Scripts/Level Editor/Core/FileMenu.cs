using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LevelEditor {
    public class FileMenu : MonoBehaviour
    {
        [SerializeField] private TMP_Dropdown _category = null;
        [SerializeField] private TMP_Dropdown _level = null;
        [SerializeField] private Button _create = null;
        [SerializeField] private Button _load = null;
        [SerializeField] private Button _save = null;
        [SerializeField] private Button _clear = null;
        [SerializeField] private Button _delete = null;

        public event Action<int> CategoryChanged;
        public event Action<int> LevelChanged;
        public event Action CreateClicked;
        public event Action LoadClicked;
        public event Action SaveClicked;
        public event Action ClearClicked;
        public event Action DeleteClicked;

        private void Awake() {
            _category.ClearOptions();
            _category.onValueChanged.AddListener(OnCategoryChanged);
            _level.ClearOptions();
            _level.onValueChanged.AddListener(OnLevelChanged);
            _create.onClick.AddListener(OnCreateClicked);
            _load.onClick.AddListener(OnLoadClicked);
            _save.onClick.AddListener(OnSaveClicked);
            _clear.onClick.AddListener(OnClearClicked);
            _delete.onClick.AddListener(OnDeleteClicked);
        }

        public void SetCategory(int index) {
            _category.SetValueWithoutNotify(index);
        }

        public void SetCategoriesList(string[] categories, int currentIndex = 0) {
            var options = new List<TMP_Dropdown.OptionData>(categories.Length);
            for (int i = 0; i < categories.Length; i++) {
                options.Add(new TMP_Dropdown.OptionData(categories[i]));
            }
            _category.options = options;
            SetCategory(currentIndex);
        }

        public void SetLevel(int index) {
            _level.SetValueWithoutNotify(index);
        }

        public void SetLevelsList(int[] levels, int currentIndex = 0) {
            var options = new List<TMP_Dropdown.OptionData>(levels.Length);
            for (int i = 0; i < levels.Length; i++) {
                options.Add(new TMP_Dropdown.OptionData(levels[i].ToString()));
            }
            _level.options = options;
            SetLevel(currentIndex);
        }

        private void OnCategoryChanged(int categoryIndex) {
            CategoryChanged?.Invoke(categoryIndex);
        }

        private void OnLevelChanged(int levelIndex) {
            LevelChanged?.Invoke(levelIndex);
        }

        private void OnCreateClicked() {
            CreateClicked?.Invoke();
        }

        private void OnLoadClicked() {
            LoadClicked?.Invoke();
        }

        private void OnSaveClicked() {
            SaveClicked?.Invoke();
        }

        private void OnClearClicked() {
            ClearClicked?.Invoke();
        }

        private void OnDeleteClicked() {
            DeleteClicked?.Invoke();
        }
    }
}
