using Common.Extensions;
using Common.Types;
using System;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Model.Crossword;

namespace LevelEditor {
    public class CharsGridEditor : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _width = null;
        [SerializeField] private TMP_InputField _height = null;
        [SerializeField] private Button _applySizeButton = null;
        [Space(20)]
        [SerializeField] private RectTransform _rectTransform = null;
        [SerializeField] private GridLayoutGroup _layoutGrid = null;
        [SerializeField, Range(0.0f, 1.0f)] private float _spacingPart = 0.1f;
        [SerializeField] private Cell _gridCellItem = null;

        public const int DefaultHeight = 6; // Rows count
        public const int DefaultWidth = 9; // Columns count

        public event Action DataChanged;

        private Cell[,] _celles;

        private void Awake() {
            _applySizeButton.onClick.AddListener(ApplySize);
        }

        public WordData[] GetWordDatas() {
            return CharTableAnalizer.GetData(_celles);
        }

        public char[,] GetData() {
            if (_celles == null) {
                return CharsGridEditor.GetDefaultGrid();
            }

            var size = _celles.GetSize();
            var result = new char[size.rows, size.columns];
            for (int i = 0; i < size.rows; i++) {
                for (int j = 0; j < size.columns; j++) {
                    result[i, j] = _celles[i, j].Text.FirstOrDefault();
                }
            }
            return result;
        }

        public void Init(char[,] grid) {
            var size = grid.GetSize();

            _width.text = size.columns.ToString();
            _height.text = size.rows.ToString();

            var rawCellWight = _rectTransform.rect.width / size.columns;
            var rawCellHeight = _rectTransform.rect.height / size.rows;
            var rawCellSide = Mathf.Min(rawCellWight, rawCellHeight);
            var spasing = rawCellSide * _spacingPart;
            var cellSide = rawCellSide - spasing;

            _layoutGrid.spacing = new Vector2(spasing, spasing);
            _layoutGrid.constraintCount = size.rows;
            var padding = ((rawCellWight - rawCellSide) * size.columns) + ((rawCellHeight - rawCellSide) * size.rows);
            padding /= 2;
            var isTopPadding = rawCellWight < rawCellHeight;
            if (isTopPadding) {
                _layoutGrid.padding = new RectOffset((int)(spasing / 2), 0, (int)padding, 0);
            }
            else {
                _layoutGrid.padding = new RectOffset((int)padding, 0, (int)(spasing / 2), 0);
            }
            _layoutGrid.cellSize = new Vector3(cellSide, cellSide);

            ActualizeCells(size);

            _gridCellItem.gameObject.SetActive(true);
            var parent = _gridCellItem.transform.parent;

            for (int i = 0; i < size.rows; i++) {
                for (int j = 0; j < size.columns; j++) {
                    var cell = _celles[i, j];

                    if (cell == false) {
                        cell = Instantiate(_gridCellItem, parent);
                        _celles[i, j] = cell;
                    }
                    
                    cell.Text = grid[i, j].ToString();
                    cell.Position = new Vector2IntSimple(i, j);
                    cell.transform.SetAsLastSibling();
                    cell.SubscribeOnInput(() => OnCellInput(cell));
                }
            }

            _gridCellItem.gameObject.SetActive(false);
            DataChanged?.Invoke();
        }

        private void OnCellInput(Cell cell) {
            var newPosition = cell.Position;
            if (Input.GetKey(KeyCode.DownArrow)) {
                newPosition.x = cell.Position.x + 1;
            }
            else {
                newPosition.y = cell.Position.y + 1;
            }

            try {
                var newCell = _celles[newPosition.x, newPosition.y];
                newCell.Select();
            }
            catch (System.Exception) {

            }
            DataChanged?.Invoke();
        }

        private void ActualizeCells((int rows, int columns) size) {
            if (_celles == null) {
                _celles = new Cell[size.rows, size.columns];
            }
            else {
                var cellsSize = _celles.GetSize();
                var needResize = cellsSize.rows != size.rows || cellsSize.columns != size.columns;

                if (needResize) {
                    var tempCells = new Cell[size.rows, size.columns];

                    var rowsToDelete = Mathf.Max(cellsSize.rows - size.rows, 0);
                    var columnsToDelete = Mathf.Max(cellsSize.columns - size.columns, 0);

                    if (rowsToDelete == 0 && columnsToDelete == 0) {
                        for (int i = 0; i < size.rows; i++) {
                            for (int j = 0; j < size.columns; j++) {
                                if (i > cellsSize.rows - 1 || j > cellsSize.columns - 1) {
                                    tempCells[i, j] = null;
                                }
                                else {
                                    tempCells[i, j] = _celles[i, j];
                                }
                            }
                        }
                    }
                    else {
                        int startI = cellsSize.rows - rowsToDelete - 1;
                        int startJ = cellsSize.columns - columnsToDelete - 1;
                        bool needDelet = false;
                        for (int i = 0; i < cellsSize.rows; i++) {
                            for (int j = 0; j < cellsSize.columns; j++) {
                                var cell = _celles[i, j];

                                if (i > startI || j > startJ) {
                                    needDelet = true;
                                }

                                if (needDelet) {
                                    Destroy(cell.gameObject);
                                    Debug.Log($"Cell was destroyed: ({i}:{j})");
                                }
                                needDelet = false;
                            }
                        }

                        for (int i = 0; i < size.rows; i++) {
                            for (int j = 0; j < size.columns; j++) {
                                tempCells[i, j] = _celles[i, j];
                            }
                        }
                    }

                    _celles = tempCells;
                }
            }
        }

        private void ApplySize() {
            if (int.TryParse(_width.text, out int width) && int.TryParse(_height.text, out int height)) {
                var size = _celles.GetSize();

                if (size.columns == width && size.rows == height) {
                    return;
                }
                else {
                    var newArray = CreateNewGrid(height, width, GetData());
                    Init(newArray);
                }
            }
        }

        private char[,] CreateNewGrid(int rows, int columns, char[,] source) {
            var newArray = new char[rows, columns];
            if (source != null && source.Length > 0) {
                var size = source.GetSize();
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < columns; j++) {
                        var oldArrayHasElementAtIndex = (i > size.rows - 1 || j > size.columns - 1) == false;
                        if (oldArrayHasElementAtIndex) {
                            newArray[i, j] = source[i, j];
                        }
                    }
                }
            }
            return newArray;
        }

        public static char[,] GetDefaultGrid() {
            return new char[DefaultHeight, DefaultWidth];
        }
    }
}
