using Common.Extensions;
using Common.Types;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LevelEditor {
    public class Cell : MonoBehaviour
    {
        [SerializeField] private Image _image = null;
        [SerializeField] private Color _empty = Color.white;
        [SerializeField] private Color _filled = Color.white;
        [SerializeField] private TMP_InputField _inputField = null;

        private event Action Input;
        private Vector2IntSimple _position;

        public string Text {
            get => _inputField.text; set {
                var isVisible = value.IsVisible();
                _image.color = isVisible ? _filled : _empty;
                _inputField.SetTextWithoutNotify(isVisible ? value : null);
            }
        }
        public Vector2IntSimple Position { get => _position; 
            set {
                _position = value;
                gameObject.name = string.Format("[{0}:{1}]", value.x, value.y);
            } }

        public void Select() {
            _inputField.Select();
        }

        private void Awake() {
            _inputField.onValueChanged.AddListener(OnEditEnded);
        }

        private void OnEditEnded(string text) {
            Text = text;
            Input?.Invoke();
        }

        public void SubscribeOnInput(Action action) {
            Input = null;
            Input += action;
        }
    }
}
