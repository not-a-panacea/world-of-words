using Common.Data;
using Model;

namespace LevelEditor {
    public class LevelContainer : DataContainer<Level> {
        public CrosswordContainer Crossword { get; set; }
        public int Time { get; set; }
        public int Steps { get; set; }
        public int Exp { get; set; }
        public bool HasCoins { get; set; }

        public LevelContainer() {
            Crossword = new CrosswordContainer();
        }

        protected override Level GetData() {
            return new Level(Crossword.Data, Time, Steps, Exp, HasCoins);
        }

        protected override void SetData(Level data) {
            Crossword.Data = data.Crossword;
            Time = data.Time;
            Steps = data.Steps;
            Exp = data.Exp;
            HasCoins = data.HasCoins;
        }
    }
}
