using System.Linq;
using Common.Extensions;
using Common.Data;
using Model;

namespace LevelEditor {
    public class CrosswordContainer : DataContainer<Crossword> {
        public char[] Letters { get; set; }
        public Crossword.WordData[] Words { get; set; }
        public char[,] Grid { get; set; }

        protected override Crossword GetData() {
            return new Crossword(Letters.Select(TextExtension.Converters.FromChar).ToArray(),
                Words, Grid.Convert(TextExtension.Converters.FromChar));
        }

        protected override void SetData(Crossword data) {
            Letters = data.Letters;
            Words = data.Words;
            Grid = data.Grid;
        }
    }
}
