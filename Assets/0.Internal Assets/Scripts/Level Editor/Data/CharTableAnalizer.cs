using Common.Extensions;
using Common.Types;
using System;
using System.Collections.Generic;
using static Model.Crossword;

namespace LevelEditor {
    public static class CharTableAnalizer
    {
        public static WordData[] GetData(Cell[,] grid) {
            if (grid == null || grid.Length == 0) {
                throw new ArgumentException(nameof(grid));
            }

            List<WordData> result = new List<WordData>();

            string word = "";
            List<Vector2IntSimple> positions = new List<Vector2IntSimple>();

            var size = grid.GetSize();

            for (int i = 0; i < size.rows; i++) {
                for (int j = 0; j < size.columns; j++) {
                    var cell = grid[i, j];
                    if (cell.Text.IsVisible()) {
                        word += cell.Text;
                        positions.Add(cell.Position);
                    }
                    else {
                        var wd2 = new WordData();
                        wd2.Word = word;
                        wd2.LetterPositions = positions.ToArray();
                        result.Add(wd2);
                        word = "";
                        positions.Clear();
                    }
                }

                var wd = new WordData();
                wd.Word = word;
                wd.LetterPositions = positions.ToArray();
                result.Add(wd);
                word = "";
                positions.Clear();
            }

            word = "";
            positions.Clear();

            for (int j = 0; j < size.columns; j++) {
                for (int i = 0; i < size.rows; i++) {
                    var cell = grid[i, j];
                    if (cell.Text.IsVisible()) {
                        word += cell.Text;
                        positions.Add(cell.Position);
                    }
                    else {
                        var wd2 = new WordData();
                        wd2.Word = word;
                        wd2.LetterPositions = positions.ToArray();
                        result.Add(wd2);
                        word = "";
                        positions.Clear();
                    }
                }

                var wd = new WordData();
                wd.Word = word;
                wd.LetterPositions = positions.ToArray();
                result.Add(wd);
                word = "";
                positions.Clear();
            }

            result.RemoveAll(wd => wd.Word.IsVisible() == false || wd.Word.Length < 2);
            return result.ToArray();
        }
    }
}
