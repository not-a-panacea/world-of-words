using Common.Data;
using Model;

namespace LevelEditor {
    public class CategoryContainer : DataContainer<Category> {
        public string Id { get; set; }
        public int SortOrder { get; set; }
        public string Name { get; set; }
        public OpeningType OpeningType { get; set; }
        public int Price { get; set; }

        protected override Category GetData() {
            return new Category(Id, SortOrder, Name, OpeningType, Price);
        }

        protected override void SetData(Category data) {
            Id = data.Id;
            SortOrder = data.SortOrder;
            Name = data.Name;
            OpeningType = data.OpeningType;
            Price = data.Price;
        }
    }
}
