using FullSerializer;
using System;
using System.Collections.Generic;

namespace Model {
    public class Profile {
        public static Profile Current => _current ?? (_current = Load());
        private static Profile _current;

        [fsProperty] private string _id;
        [fsProperty] private Dictionary<string, int> _categoriesProgress = new Dictionary<string, int>();
        [fsProperty] private List<string> _openedCategories;

        public event Action<string> CategoryOpened;
        public event Action<string, int> LevelCompleted;

        public string ID => _id;

        private Profile() {
            _id = "player_id";
            _openedCategories = new List<string>();
        }

        public int GetProgress(string categoryID) {
            int levelIndex;
            if (_categoriesProgress.TryGetValue(categoryID, out levelIndex)) {

            }
            return levelIndex;
        }

        public void AddProgress(string categoryID) {
            int levelIndex;
            _categoriesProgress.TryGetValue(categoryID, out levelIndex);
            levelIndex++;
            _categoriesProgress[categoryID] = levelIndex;
            Save();
            LevelCompleted?.Invoke(categoryID, levelIndex);
        }

        public void CategoryOpen(string categoryID) {
            if (_openedCategories.Contains(categoryID) == false) {
                _openedCategories.Add(categoryID);
                Save();
                CategoryOpened?.Invoke(categoryID);
            }
        }

        public bool IsCategoryOpened(string name) {
            return _openedCategories.Contains(name);
        }

        public void Save() {
            Save(this);
        }

        public static Profile CreateDefault() {
            return new Profile();
        }

        public static Profile Load() {
            var profile = GameDataLoader.LoadProfile();
            if (profile == null) {
                profile = CreateDefault();
            }
            return profile;
        }

        public static void Save(Profile profile) {
            GameDataLoader.SaveProfile(profile);
        }
    }
}
