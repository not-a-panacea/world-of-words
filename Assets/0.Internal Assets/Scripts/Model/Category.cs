using FullSerializer;
using System;

namespace Model {
    [Serializable]
    public class Category : IComparable<Category> {
        [fsProperty] private readonly string _id;
        [fsProperty] private readonly int _sortOrder;
        [fsProperty] private readonly string _name;
        [fsProperty] private readonly OpeningType _openingType;
        [fsProperty] private readonly int _price;

        public Category(string id, int sortOrder, string name, OpeningType openingType, int price) {
            _id = id;
            _sortOrder = sortOrder;
            _name = name;
            _openingType = openingType;
            _price = price;
        }

        public string Id => _id;
        public int SortOrder => _sortOrder;
        public string Name => _name;
        public OpeningType OpeningType => _openingType;
        public int Price => _price;

        public int CompareTo(Category other) {
            return -_sortOrder.CompareTo(other._sortOrder);
        }
    }
}
