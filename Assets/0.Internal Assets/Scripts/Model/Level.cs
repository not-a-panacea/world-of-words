using FullSerializer;
using System;

namespace Model {
    [Serializable]
    public class Level {
        [fsProperty] private readonly Crossword _crossword;
        [fsProperty] private readonly int _time; // seconds
        [fsProperty] private readonly int _steps;
        [fsProperty] private readonly int _exp;
        [fsProperty] private readonly bool _hasCoins;

        public Level(Crossword crossword) : this(crossword, -1, -1, 0, false) {
        }

        public Level(Crossword crossword, int time, int steps, int exp, bool hasCoins) {
            _crossword = crossword;
            _time = time;
            _steps = steps;
            _exp = exp;
            _hasCoins = hasCoins;
        }

        public Crossword Crossword => _crossword;
        public int Time => _time;
        public int Steps => _steps;
        public int Exp => _exp;
        public bool HasCoins => _hasCoins;
    }
}
