using Common.Collections;
using Common.Extensions;
using Common.Types;
using FullSerializer;
using System;
using System.Linq;

namespace Model {
    [Serializable]
    public class Crossword {
        [fsProperty] private readonly string[] _letters;
        [fsProperty] private readonly WordData[] _words;
        [fsProperty] private readonly Table<string> _grid;

        public Crossword() {
            _letters = Array.Empty<string>();
            _words = Array.Empty<WordData>();
            _grid = new Table<string>();
        }

        public Crossword(WordData[] words, string[,] grid) :
            this(words.Select(wd => wd.Word).GetUnicCharsWithRepeating(), words, grid) {
        }

        public Crossword(string[] letters, WordData[] words, string[,] grid) {
            _letters = letters;
            _words = words;
            _grid = new Table<string>(grid);
        }

        public char[] Letters => _letters.Select(TextExtension.Converters.FromString)
            .Where(ch => ch != default).ToArray();
        public WordData[] Words => _words;
        public Table<char> Grid => Table<string>.Convert(_grid, TextExtension.Converters.FromString);

        [Serializable]
        public struct WordData {
            [fsProperty] public string Word;
            [fsProperty] public Vector2IntSimple[] LetterPositions;

            public static implicit operator WordData((string word, Vector2IntSimple[] letterPositions) tuple) {
                return new WordData() { Word = tuple.word, LetterPositions = tuple.letterPositions };
            }
        }
    }
}