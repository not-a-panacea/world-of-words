using EconomySystem;
using System.Collections.Generic;

namespace Model {
    public static class EconomyExtension {
        public static IEnumerable<Account> GetAccounts(this Profile profile) {
            return EconomySystem.Economy.GetAccounts(profile.ID);
        }
    }
}
