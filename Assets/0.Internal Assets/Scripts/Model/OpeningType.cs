﻿using System;

namespace Model {
    [Serializable]
    public enum OpeningType {
        Unavailable,
        Opened,
        Coins,
        ADS
    }
}
