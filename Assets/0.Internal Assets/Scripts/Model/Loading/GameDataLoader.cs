using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Model {
    public static class GameDataLoader {
        private const string DataFolder = "Levels/";
        private const string DefaultCategoryFolder = "Mix/";
        private const string CategoriesFile = "Categories";
        private const string LevelDataFile = " Level";

        private static Func<int, string> _getLevelFileName = index => string.Format("{0}{1}", index, LevelDataFile);
        public static string GetLevelName(int index) => _getLevelFileName(index);

        public static string FullLevelsPath {
            get {
                if (string.IsNullOrEmpty(_fullLevelsPath))
                    _fullLevelsPath = Constants.Paths.ResourcesFull + DataFolder;

                return _fullLevelsPath;
            }
        }
        private static string _fullLevelsPath;

        public static bool HasData => Directory.Exists(FullLevelsPath);

        public static void InitDataFolder() {
            Directory.CreateDirectory(FullLevelsPath);

            var defaultCategoryPath = Path.Combine(FullLevelsPath, DefaultCategoryFolder);
            Directory.CreateDirectory(defaultCategoryPath);
        }

        public static void SaveCategoriesData(List<Category> categories) {
            SaveLoader.SaveJsonToResources(DataFolder, CategoriesFile, categories);
        }

        public static void SaveLevel(string category, int levelIndex, Level level) {
            SaveLoader.SaveJsonToResources(DataFolder + category, GetLevelName(levelIndex), level);
        }

        public static List<Category> LoadCategoriesData() {
            return SaveLoader.LoadJsonFromResources<List<Category>>(DataFolder + CategoriesFile);
        }

        public static List<string> LoadLevels(string category) {
            var categoryPath = Path.Combine(FullLevelsPath, category);

            var levels = Directory.EnumerateFiles(categoryPath)
                .Where(fn => fn.Contains(Constants.FileExtensions.Meta) == false)
                .ToList();
            return levels;
        }

        public static Level LoadLevel(string category, int levelIndex) {
            return SaveLoader.LoadJsonFromResources<Level>(string.Format("{0}/{1}", DataFolder + category, GetLevelName(levelIndex)));
        }

        #region Profile
        public static void SaveProfile(Profile profile) {
            SaveLoader.SaveJsonToInternalStorage(profile);
        }

        public static Profile LoadProfile() {
            return SaveLoader.LoadJsonToInternalStorage<Profile>(typeof(Profile).Name);
        }
        #endregion
    }
}
