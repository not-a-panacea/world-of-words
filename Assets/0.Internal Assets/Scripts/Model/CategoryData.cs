﻿namespace Model {
    public struct CategoryData {
        public Category Category;
        public int LevelsCount;
        public int CompletedLevels;
        public bool IsOpen;
        public bool IsCompleted => CompletedLevels >= LevelsCount;
    }
}
