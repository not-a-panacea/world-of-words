#if UNITY_EDITOR
using System.IO;
using UnityEditor;

namespace Model {
    public static class ModelUnityTools
    {
        [MenuItem("Tools/Clear Saves")]
        private static void ClrearSaves() {
            Directory.Delete(Constants.Paths.InternalStorage, true);
        }
    }
}
#endif