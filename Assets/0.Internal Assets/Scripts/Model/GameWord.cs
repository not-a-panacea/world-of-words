using Common.Types;
using static Model.Crossword;
using System.Linq;

namespace Model {
    public readonly struct GameWord {
        public readonly GameWordLetter[] Data;

        public int Length => Data.Length;
        public string Word => Data.Select(gwl => gwl.Letter).ToArray().ToString();
        public Vector2IntSimple[] Positions => Data.Select(gwl => gwl.Position).ToArray();
        public bool IsOpen => Data.All(gwl => gwl.IsOpen);

        public GameWord(WordData wordData, bool[] openedPositions) {
            Data = new GameWordLetter[wordData.LetterPositions.Length];
            for (int i = 0; i < wordData.LetterPositions.Length; i++) {
                Data[i] = new GameWordLetter(wordData.Word[i], wordData.LetterPositions[i], openedPositions[i]);
            }
        }
    }

    public readonly struct GameWordLetter {
        public readonly char Letter;
        public readonly Vector2IntSimple Position;
        public readonly bool IsOpen;

        public GameWordLetter(char letter, Vector2IntSimple position, bool isOpen) {
            Letter = letter;
            Position = position;
            IsOpen = isOpen;
        }
    }
}
