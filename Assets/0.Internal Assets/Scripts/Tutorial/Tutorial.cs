using Common.Extensions;
using Common.Streams;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TutorialSystem {
    public static class Tutorial
    {
        private static Step _step = Tutorial.Step.Start;
        private static bool _initialized = false;
        private static List<Step> _steps;
        private static Dictionary<Step, int> _stepRejects = new Dictionary<Step, int>();
        private static int _subscribersCount;

        public static Step CurrentStep => _step;
        public static bool IsCompleted => CurrentStep >= Step.End;

        public static Step GetNext(Step step) {
            var index = _steps.IndexOf(step);
            index++;
            index = _steps.LoopIndex(index);
            return _steps[index];
        }

        public static void Init() {
            if (_initialized) return;
            _steps = Enum.GetValues(typeof(Step)).Cast<Step>().ToList();
            LoadData();
            _initialized = true;
        }

        public static void Start() {
            Init();
            if (IsCompleted) {
                return;
            }
            StreamsDispatcher.Start<Step>(CurrentStep, true);
        }

        public static void Skip() {
            StreamsDispatcher.Stop<Step>();
        }

        public static void Subscribe(ITutorialStepHandler handler, Step stepToHandle) {
            Subscribe(handler, -(int)stepToHandle);
        }

        private static void Subscribe(ITutorialStepHandler handler, int priority) {
            var listener = new StreamListener(handler, priority);
            listener.Completed += result => OnStepCompleted(listener, result);
            _subscribersCount++;
            StreamsDispatcher.Subscribe<Step>(listener);
        }

        private static void OnStepCompleted(StreamListener listener, IListenerWorkResult result) {
            ListenerWorkResult handlerResult = result as ListenerWorkResult;
            _step = handlerResult.Data;
            if (handlerResult.Success == false && IsCompleted == false) {
                AddStepReject(_step);
                if (ShouldSkipStep(_step)) {
                    _step = GetNext(_step);
                }
                Subscribe(listener.Handler, listener.Priority - 1);
            }
            SaveData();
            _subscribersCount--;
        }

        private static void LoadData() {
            _step = SaveLoader.LoadJsonToInternalStorage<Step>(nameof(Tutorial));
        }

        private static void SaveData() {
            SaveLoader.SaveJsonToInternalStorage(CurrentStep, nameof(Tutorial));
        }

        private static bool ShouldSkipStep(Step step) {
            _stepRejects.TryGetValue(step, out int rejectsCount);
            return rejectsCount >= _subscribersCount;
        }

        private static void AddStepReject(Step step) {
            _stepRejects.TryGetValue(step, out int rejectsCount);
            rejectsCount++;
            _stepRejects[step] = rejectsCount;
        }

        #region InternalTypes
        public enum Step {
            Start = 0,
            CategoryPlay = 10,
            CategoryPassed = 20,
            End = 100
        }

        private class StreamListener : IStreamListener<Step> , IDisposable{
            private readonly ITutorialStepHandler _handler;

            public StreamListener(ITutorialStepHandler handler, int priority) {
                _handler = handler;
                Priority = priority;
            }

            public event Action<IListenerWorkResult> Completed;

            public int Priority { get; private set; }

            public ITutorialStepHandler Handler => _handler;

            public void StartWork(Step step, IListenerWorkResult prewResult) {
                Step nextStep = Tutorial.CurrentStep;
                if (prewResult != null && prewResult is IListenerWorkResult<Step> resultWithStep) {
                    if (resultWithStep.Data > nextStep) nextStep = resultWithStep.Data;
                }
                Handler.HandleTutorialStep(nextStep, OnStepHandlingCompleted);
            }

            private void OnStepHandlingCompleted(bool handlingSuccessful, Step nextStep) {
                if (nextStep < Tutorial.CurrentStep) {
                    throw new InvalidOperationException("Invalid next tutorial step");
                }

                Completed?.Invoke(new ListenerWorkResult(handlingSuccessful, nextStep));
            }

            public void Dispose() {
                Completed = null;
            }
        }

        private class ListenerWorkResult : IListenerWorkResult<Step> {
            private readonly Step _step;
            private readonly bool _success;

            public ListenerWorkResult(bool success, Step step) {
                _success = success;
                _step = step;
            }

            public Step Data => _step;

            public bool Success => _success;

            object IListenerWorkResult.Data => _step;
        }
        #endregion
    }
}
