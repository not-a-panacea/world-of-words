﻿using System;
using static TutorialSystem.Tutorial;

namespace TutorialSystem {
    public interface ITutorialStepHandler {
        void HandleTutorialStep(Step step, Action<bool, Step> handlingCompleted);
    }

    public class DelegateTutorialStepHandler : ITutorialStepHandler {
        private Action<Step, Action<bool, Step>> _handler;

        public DelegateTutorialStepHandler(Action<Step, Action<bool, Step>> handler) {
            _handler = handler;
        }

        public void HandleTutorialStep(Step step, Action<bool, Step> handlingCompleted) {
            _handler(step, handlingCompleted);
        }
    }
}
