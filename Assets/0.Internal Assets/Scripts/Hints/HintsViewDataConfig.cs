using UnityEngine;
using System.Linq;
using Common;

namespace Hints {
    public class HintsViewDataConfig : ScriptableSettings<HintsViewDataConfig> {
        [SerializeField] private HintViewData[] _viewDatas = null;

        public HintViewData GetViewData(string systemName) {
            return _viewDatas.First(vd => vd.IsMatch(systemName));
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem(menuItemPrefix + nameof(HintsViewDataConfig))]
        private static void SelectSettingsAsset() {
            SelectSettings(Current);
        }
#endif
    }
}
