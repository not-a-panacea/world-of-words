using Common.Extensions;
using System.Linq;

namespace Hints {
    public sealed class OpentLetterHint : Hint {
        internal OpentLetterHint(HintViewData viewData, HintsStorage storage) : base(viewData, storage) {

        }

        protected override void OnUse(IGameField gameField) {
            var data = gameField.GetGameData();
            var variants = data.Where(w => w.IsOpen == false)
                .SelectMany(w => w.Data)
                .Where(l => l.IsOpen == false)
                .ToArray();
            var random = variants.GetRandomItem();
            gameField.OpenPositions(random.Position);
        }
    }
}
