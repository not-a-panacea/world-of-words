using Common.Types;
using Model;
using System.Collections.Generic;

namespace Hints {
    public interface IGameField {
        IEnumerable<GameWord> GetGameData();
        void OpenPositions(params Vector2IntSimple[] positions);
    }
}
