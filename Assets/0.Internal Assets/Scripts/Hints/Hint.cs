using System;
using UnityEngine;

namespace Hints {
    public abstract class Hint : IShowable
    {
        protected HintViewData _viewData;
        private readonly HintsStorage _storage;

        protected Hint(HintViewData viewData, HintsStorage storage) {
            if (viewData == null) {
                throw new ArgumentNullException(nameof(viewData));
            }
            if (storage == null) {
                throw new ArgumentNullException(nameof(storage));
            }
            _viewData = viewData;
            _storage = storage;
        }

        public event Action Used;

        public HintViewData ViewData => _viewData;
        public string Info => ViewData.Info;
        public Sprite Icon => ViewData.Icon;

        public void Use(IGameField gameField) {
            _storage.Substract(this);
            OnUse(gameField);
            Used?.Invoke();
        }
        protected abstract void OnUse(IGameField gameField);
    }
}
