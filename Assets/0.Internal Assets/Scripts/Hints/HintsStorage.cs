using Common.Collections;
using System;
using System.Collections.Generic;
using FullSerializer;
using Common;

namespace Hints {
    public sealed class HintsStorage {
        #region Static
        public static event Action<(Hint hint, int prevCount, int currentCount, int delta)> HintCountChanged;
        public static HintsStorage Instance => _instance ?? (_instance = LoadOrCreate());

        private static OpentLetterHint _opentLetterHint;
        private static OpenWordHint _opentWordHint;

        private static HintsStorage _instance;

        private static void Save() {
            SaveLoader.SaveJsonToInternalStorage(_instance);
        }

        private static HintsStorage LoadOrCreate() {
            var data = SaveLoader.LoadJsonToInternalStorage<HintsStorage>(nameof(HintsStorage));
            if (data == null) {
                data = new HintsStorage();
            }
            MobileSystem.SavingData += Save;
            return data;
        }
        #endregion

        [fsProperty] private SimpleStorage _storage;

        private HintsStorage() {
            var viewDatas = HintsViewDataConfig.Current;
            _opentLetterHint = new OpentLetterHint(viewDatas.GetViewData(typeof(OpentLetterHint).Name), this);
            _opentWordHint = new OpenWordHint(viewDatas.GetViewData(typeof(OpenWordHint).Name), this);
            _storage = new SimpleStorage();
        }

        public IEnumerable<(Hint hint, int count)> GetContent() {
            yield return (_opentLetterHint, GetCount(_opentLetterHint.Info));
            yield return (_opentWordHint, GetCount(_opentWordHint.Info));
        }

        public void Add(Hint hint, int count = 1) {
            ChangeCount(hint, count);
        }

        public void Substract(Hint hint, int count = 1) {
            ChangeCount(hint, -count);
        }

        private void ChangeCount(Hint hint, int count) {
            CheckArguments(hint, Math.Abs(count));

            string hintName = hint.Info;
            int prevCount = GetCount(hint);
            if (count > 0) {
                _storage.Add(hintName, count);
            }
            else {
                _storage.Substract(hintName, -count);
            }
            int currentCount = GetCount(hintName);
            HintCountChanged?.Invoke((hint, prevCount, currentCount, count));
        }

        private void CheckArguments(Hint hint, int count) {
            if (hint == null) {
                throw new NullReferenceException("Hint is NULL");
            }
            if (count <= 0) {
                throw new ArgumentOutOfRangeException($"Invalid count: {count}");
            }
        }

        public int GetCount(Hint hint) {
            return GetCount(hint.Info);
        }

        private int GetCount(string hintName) {
            return CheckNameAndDo<int>(hintName, () => _storage.GetCount(hintName));
        }

        private void CheckNameAndDo(string hintName, Action action) {
            if (IsValidName(hintName)) {
                action();
            }
            else {
                throw new ArgumentException($"Ivalid hint name: {hintName}");
            }
        }

        private T CheckNameAndDo<T>(string hintName, Func<T> action) {
            if (IsValidName(hintName)) {
                return action();
            }
            else {
                throw new ArgumentException("Ivalid hint name");
            }
        }

        private bool IsValidName(string hintName) {
            return _opentLetterHint.Info == hintName || _opentWordHint.Info == hintName;
        }
    }
}
