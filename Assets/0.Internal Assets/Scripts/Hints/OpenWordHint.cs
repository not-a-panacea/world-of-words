using Common.Extensions;
using System.Linq;

namespace Hints {
    public sealed class OpenWordHint : Hint {
        internal OpenWordHint(HintViewData viewData, HintsStorage storage) : base(viewData, storage) {

        }

        protected override void OnUse(IGameField gameField) {
            var data = gameField.GetGameData();
            var variants = data.Where(w => w.IsOpen == false).ToArray();
            var random = variants.GetRandomItem();
            gameField.OpenPositions(random.Positions);
        }
    }
}
