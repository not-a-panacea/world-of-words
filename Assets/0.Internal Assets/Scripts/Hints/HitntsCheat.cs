using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Hints {
    public class HitntsCheat : MonoBehaviour {
        [SerializeField] private Button _button = null;
        [SerializeField] private GameObject _cheatPanel = null;
        [SerializeField] private Button _addHint1 = null;
        [SerializeField] private Button _addHint2 = null;

        private void Start() {
            if (Debug.isDebugBuild == false) {
                gameObject.SetActive(false);
                return;
            }

            _button.onClick.AddListener(OpenClosePanel);
            AssignHintsToButton(HintsStorage.Instance.GetContent());
        }

        private void OpenClosePanel() {
            _cheatPanel.SetActive(!_cheatPanel.activeSelf);
        }

        private void AssignHintsToButton(IEnumerable<(Hint hint, int count)> content) {
            int i = 0;
            foreach (var pair in content) {
                if (i == 0) {
                    _addHint1.onClick.AddListener(() => AddHint(pair.hint));
                }
                else if (i == 1){
                    _addHint2.onClick.AddListener(() => AddHint(pair.hint));
                }
                i++;
            }
        }

        private void AddHint(Hint hint) {
            HintsStorage.Instance.Add(hint);
        }
    }
}
