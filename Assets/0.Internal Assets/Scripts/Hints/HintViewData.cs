using System;
using UnityEngine;

namespace Hints {
    [Serializable]
    public sealed class HintViewData : IShowable {
        [SerializeField] private string _systemName = "";
        [SerializeField] private string _name = "";
        [SerializeField] private Sprite _icon = null;

        public string Info => _name;
        public Sprite Icon => _icon;

        public bool IsMatch(string systemName) {
            return string.Compare(systemName, _systemName, true) == 0;
        }
    }
}
