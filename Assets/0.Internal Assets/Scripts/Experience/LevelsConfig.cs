using Common;
using System;
using UnityEngine;

namespace ExperienceSystem {
    public class LevelsConfig : ScriptableSettings<LevelsConfig> {
        [SerializeField] private Level[] _levels = new Level[0];

        public Level GetLevel(int expCount) {
            return _levels[GetLevelInfo(expCount).LevelIndex];
        }

        public float GetProgress(int expCount) {
            var currentLevelInfo = GetLevelInfo(expCount);
            bool isMaxLevel = currentLevelInfo.LevelIndex >= _levels.Length - 1;
            if (isMaxLevel) {
                return 1;
            }
            else {
                var nextIndex = currentLevelInfo.LevelIndex + 1;
                var nextLevel = _levels[nextIndex];
                float expCountAtCurrentLevel = expCount - currentLevelInfo.LevelExpCount;
                return expCountAtCurrentLevel / nextLevel.RequiredExp;
            }
        }

        public LevelInfo GetLevelInfo(int expCount) {
            if (_levels == null || _levels.Length == 0) {
                throw new NullReferenceException(nameof(_levels));
            }

            int totalExp = 0;
            int index = 0;

            for (int i = 0; i < _levels.Length; i++) {
                totalExp += _levels[i].RequiredExp;
                if (expCount >= totalExp) {
                    index = i;
                }
                else {
                    totalExp -= _levels[i].RequiredExp;
                    break;
                }
            }

            return new LevelInfo(index, totalExp);
        }

        public readonly struct LevelInfo {
            public readonly int LevelIndex;
            public readonly int LevelExpCount;

            public LevelInfo(int levelIndex, int levelExpCount) {
                LevelIndex = levelIndex;
                LevelExpCount = levelExpCount;
            }
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem(menuItemPrefix + nameof(LevelsConfig))]
        private static void SelectSettingsAsset() {
            SelectSettings(Current);
        }
#endif
    }
}
