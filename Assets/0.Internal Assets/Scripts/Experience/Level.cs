﻿using UnityEngine;
using System;

namespace ExperienceSystem {
    [Serializable]
    public struct Level {
        [SerializeField] private string _name;
        [SerializeField] private int _requiredExp;

        public string Name { get => _name; }
        public int RequiredExp { get => _requiredExp; }
    }
}
