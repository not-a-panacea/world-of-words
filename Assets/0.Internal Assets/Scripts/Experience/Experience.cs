using FullSerializer;
using System;

namespace ExperienceSystem {
    public class Experience
    {
        private static Experience _instance;
        public static Experience Instance => _instance ?? (_instance = LoadOrCreate());

        public static event Action<(float prev, float current, float delta)> ExpCountChanged;

        [fsProperty] private float _expCount;

        private Experience() {
            _expCount = 0;
        }

        private static Experience LoadOrCreate() {
            var exp = SaveLoader.LoadJsonToInternalStorage<Experience>(nameof(Experience));
            if (exp == null) {
                exp = new Experience();
            }
            return exp;
        }

        public static void Add(float value) {
            if (value < 0) {
                throw new ArgumentException("Value < 0");
            }

            var prevValue = Instance._expCount;

            Instance._expCount += value;
            Save();
            ExpCountChanged?.Invoke((prevValue, Instance._expCount, value));
        }

        public static Level GetCurrentLevel() {
            return LevelsConfig.Current.GetLevel((int)Instance._expCount);
        }

        public static float GetProgress() {
            return LevelsConfig.Current.GetProgress((int)Instance._expCount);
        }

        public static void Save() {
            SaveLoader.SaveJsonToInternalStorage(_instance);
        }
    }
}
