﻿using System;
using UnityEngine;

namespace EconomySystem {
    [Serializable]
    public struct Account {
        public readonly string CurrencyID;
        public int Value;

        public Account(string currencyID) {
            CurrencyID = currencyID;
            Value = 0;
        }

        public TransactionResult ApplyTransaction(Transaction transaction) {
            if (string.Compare(transaction.CurrencyID, CurrencyID, true) == 0) {
                var sign = Mathf.Sign(transaction.Value);
                var success = sign < 0 ? Value >= Mathf.Abs(transaction.Value) : true;
                if (success) Value += transaction.Value;
                return new TransactionResult(transaction, success);
            }
            else {
                throw new InvalidOperationException("Invalid currency id");
            }
        }

        public bool IsValid() {
            return string.IsNullOrEmpty(CurrencyID) == false && Value <= -1 == false;
        }
    }
}
