using FullSerializer;
using System;
using System.Collections.Generic;

namespace EconomySystem {
    public static class Economy {
        public static event Action<string, TransactionResult> TransactionProvided;

        private static EconomyData Data => _data ?? (_data = LoadOrCreateData());
        private static EconomyData _data;
        private static HashSet<string> _currencies = new HashSet<string>();
        private static Func<string, string, string> _idCombiner = (profileID, currencyID) => $"{profileID}-{currencyID}";

        public static TransactionResult ProvideTransaction(string profileID, Transaction transaction) {
            if (_currencies.Contains(transaction.CurrencyID) == false) {
                throw new InvalidOperationException("Unknow currency!");
            }

            var id = _idCombiner(profileID, transaction.CurrencyID);
            var account = Data.GetAccount(id);
            if (account.IsValid() == false) {
                account = new Account(transaction.CurrencyID);
            }
            var result = account.ApplyTransaction(transaction);
            if (result.Success) {
                Data.UpdateData(id, account);
                Save();
            }
            TransactionProvided?.Invoke(profileID, result);
            return result;
        }

        public static IEnumerable<Account> GetAccounts(string profileID) {
            foreach (var currency in _currencies) {
                var id = _idCombiner(profileID, currency);
                yield return Data.GetAccount(id);
            }
        }

        public static void ResgisterAllCurrencies(params string[] currencies) {
            foreach (var item in currencies) {
                _currencies.Add(item);
            }
        }

        private static EconomyData LoadOrCreateData() {
            var data = SaveLoader.LoadJsonToInternalStorage<EconomyData>(nameof(EconomyData));
            if (data == null) {
                data = new EconomyData();
            }
            return data;
        }

        private static void Save() {
            SaveLoader.SaveJsonToInternalStorage(_data);
        }
    }

    public class EconomyData {
        [fsProperty] private Dictionary<string, Account> _accounts = new Dictionary<string, Account>();

        public Account GetAccount(string id) {
            _accounts.TryGetValue(id, out var account);
            return account;
        }

        public void UpdateData(string id, Account account) {
            _accounts[id] = account;
        }
    }
}
