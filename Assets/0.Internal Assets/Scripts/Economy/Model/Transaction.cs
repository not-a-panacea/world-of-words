﻿namespace EconomySystem {
    public readonly struct Transaction {
        public readonly string CurrencyID;
        public readonly int Value;

        public Transaction(string currencyID, int value) {
            CurrencyID = currencyID;
            Value = value;
        }
    }
}
