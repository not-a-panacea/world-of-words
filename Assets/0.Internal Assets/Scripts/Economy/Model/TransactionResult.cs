﻿namespace EconomySystem {
    public readonly struct TransactionResult {
        public readonly Transaction Transaction;
        public readonly bool Success;

        public TransactionResult(Transaction transaction, bool success) {
            Transaction = transaction;
            Success = success;
        }
    }
}
