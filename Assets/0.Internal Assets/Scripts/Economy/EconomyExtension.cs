namespace EconomySystem {
    public static class EconomyExtension {
        public static Transaction CreateTransaction(this Price price) {
            return new Transaction(price.Currency.Id, -price.Value);
        }
    }
}
