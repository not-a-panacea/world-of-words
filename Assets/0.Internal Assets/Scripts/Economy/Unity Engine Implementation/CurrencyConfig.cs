﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using Common;

namespace EconomySystem {
    public class CurrencyConfig : ScriptableSettings<CurrencyConfig> {
        [SerializeField] private string _defaultCurrencyId = "Coin";
        [SerializeField] private Currency[] _currencies = new Currency[0];

        public IEnumerable<Currency> Currencies => _currencies;

        public string DefaultCurrencyId { get => _defaultCurrencyId; }

        public Currency GetCurrency(string id) {
            return _currencies.FirstOrDefault(c => string.Compare(c.Id, id, true) == 0);
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem(menuItemPrefix + nameof(CurrencyConfig))]
        private static void SelectSettingsAsset() {
            SelectSettings(Current);
        }
#endif
    }
}
