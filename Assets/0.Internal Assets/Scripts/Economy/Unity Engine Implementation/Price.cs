﻿using System;
using UnityEngine;

namespace EconomySystem {
    [Serializable]
    public class Price : IShowable {
        [SerializeField] private Currency _currency = null;
        [SerializeField, Min(0)] private int _value = 0;

        public string Info => _value.ToString();
        public Sprite Icon => _currency.Icon;

        public int Value { get => _value; }
        public Currency Currency { get => _currency; }

        public Price(Currency currency, int value) {
            _currency = currency ?? throw new NullReferenceException(nameof(currency));
            _value = Mathf.Abs(value);
        }
    }
}
