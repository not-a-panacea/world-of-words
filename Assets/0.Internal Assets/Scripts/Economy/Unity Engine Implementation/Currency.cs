using UnityEngine;

namespace EconomySystem {
    [CreateAssetMenu(fileName = nameof(Currency), menuName = "Configs/Economy/" + nameof(Currency))]
    public class Currency : ScriptableObject, IShowable {
        [SerializeField] private string _id = "";
        [SerializeField] private string _name = "";
        [SerializeField] private string _shortName = "";
        [SerializeField] private Sprite _icon = null;

        public string Id { get => _id; }
        public string Name { get => _name; }
        public string ShortName { get => _shortName; }
        public Sprite Icon { get => _icon; }
        public string Info => Name;
    }
}
