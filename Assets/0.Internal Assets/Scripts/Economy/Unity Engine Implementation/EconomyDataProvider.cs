using System.Linq;
using UnityEngine;

namespace EconomySystem {
    public class EconomyDataProvider : MonoBehaviour {
        public static CurrencyConfig CurrencyConfig { get; private set; }

        [SerializeField] private CurrencyConfig _currencyConfig = null;

        private void Awake() {
            var currenciesNames = _currencyConfig.Currencies.Select(c => c.Id).ToArray();
            Economy.ResgisterAllCurrencies(currenciesNames);
            CurrencyConfig = _currencyConfig;
        }
    }
}
