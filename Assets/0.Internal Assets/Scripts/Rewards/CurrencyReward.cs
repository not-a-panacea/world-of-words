using EconomySystem;
using Model;
using UnityEngine;

namespace Rewards {
    [CreateAssetMenu(fileName = nameof(CurrencyReward), menuName = "Configs/Economy/Rewards" + nameof(CurrencyReward))]
    public class CurrencyReward : Reward {
        [SerializeField] private Currency _currency = null;
        [SerializeField, Min(0)] private int _value = 0;

        public override string Info => _value.ToString();
        public override Sprite Icon => _currency.Icon;
        public int Value  => _value;

        public override void Gein() {
            var transaction = new Transaction(_currency.Id, _value);
            Economy.ProvideTransaction(Profile.Current.ID, transaction);
        }

        public static CurrencyReward Create(string currencyID, int value) {
            var currency = EconomyDataProvider.CurrencyConfig.GetCurrency(currencyID);
            return Create(currency, value);
        }

        public static CurrencyReward Create(Currency currency, int value) {
            var reward = ScriptableObject.CreateInstance<CurrencyReward>();
            reward._currency = currency;
            reward._value = value;
            return reward;
        }
    }
}
