using UnityEngine;

namespace Rewards {
    public abstract class Reward : ScriptableObject, IShowable {
        public abstract string Info { get; }
        public abstract Sprite Icon { get; }

        public abstract void Gein();
    }
}
