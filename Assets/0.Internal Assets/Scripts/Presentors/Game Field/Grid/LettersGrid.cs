using Common.Types;
using UnityEngine;
using Views;

namespace Presentors {
    public class LettersGrid : MonoBehaviour {
        [SerializeField] private LettersGridView _view = null;

        public Vector2 CellSize => _view.CellSize;

        public void Init(char[,] grid) {
            _view.Init(grid);
        }

        public void ShowPositions(Vector2IntSimple[] positions) {
            _view.ShowPositions(positions);
        }

        public Vector3 GetWorldPosition(Vector2IntSimple charPosition) {
            return _view.GetCharPosition(charPosition);
        }
    }
}