using Common.Extensions;
using Common.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Views;

namespace Presentors {
    public class CoinsPlacer : MonoBehaviour {
        [SerializeField] private LettersGrid _lettersGrid = null;
        [SerializeField] private CoinView _coinViewPrefab = null;
        [SerializeField] private Transform _coinsContainer = null;

        private CoinView[] _coins;
        private List<CoinView> _finedCoins = new List<CoinView>(1);
        private HashSet<CoinView> _alreadyFinded = new HashSet<CoinView>();

        public event Action<int> CoinsFinded;

        public void Init(Vector2IntSimple[] positions) {
            _coins = new CoinView[positions.Length];
            _finedCoins = new List<CoinView>(positions.Length);
            _coinViewPrefab.SetSize(_lettersGrid.CellSize);
            for (int i = 0; i < positions.Length; i++) {
                var charPosition = positions[i];
                var worldPosition = _lettersGrid.GetWorldPosition(charPosition);
                var coin = Instantiate(_coinViewPrefab, _coinsContainer);
                coin.transform.position = worldPosition;
                coin.RelativePosition = charPosition;
                _coins[i] = coin;
            }
            _coinViewPrefab.gameObject.SetActive(false);
        }

        public void Hide() {
            _coinsContainer.gameObject.SetActive(false);
        }

        public void PositionOpened(Vector2IntSimple position) {
            PositionsOpened(position.ToSingleElementArray());
        }

        public void PositionsOpened(Vector2IntSimple[] positions) {
            _finedCoins.Clear();
            for (int i = 0; i < positions.Length; i++) {
                var charPosition = positions[i];
                if (TryFindCoin(charPosition, out var coin)) {
                    _finedCoins.Add(coin);
                }
            }

            if (_finedCoins.Count > 0) {
                foreach (var coin in _finedCoins) {
                    coin.Hide();
                }
                CoinsFinded?.Invoke(_finedCoins.Count);
            }
        }

        private bool TryFindCoin(Vector2IntSimple position, out CoinView coin) {
            coin = default;
            if (_coins == null || _coins.Length == 0) return false;
            coin = _coins.FirstOrDefault(c => c.RelativePosition.Equals(position));
            bool notNull = coin != null;
            return _alreadyFinded.Add(coin) && notNull;
        }
    }
}
