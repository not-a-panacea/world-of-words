using Hints;
using System;
using System.Collections.Generic;
using UnityEngine;
using Views;

namespace Presentors {
    public class HintsPresentor : MonoBehaviour {
        [SerializeField] private HintView _firstHintView = null;
        [SerializeField] private HintView _secondHintView = null;

        public event Action<Hint> HintClick;

        private Dictionary<Hint, HintView> _views = new Dictionary<Hint, HintView>();

        public void Assign(IEnumerable<(Hint hint, int count)> hints) {
            if (_views.Count > 0) {
                _views.Clear();
                HintsStorage.HintCountChanged -= OnHintCountChanged;
            }
            
            int i = 0;
            string countInfo = "";
            foreach (var pair in hints) {
                countInfo = GetCountInfo(pair);
                if (i == 0) {
                    _firstHintView.Show((pair.hint, countInfo));
                    _views.Add(pair.hint, _firstHintView);
                }
                else if (i == 1) {
                    _secondHintView.Show((pair.hint, countInfo));
                    _views.Add(pair.hint, _secondHintView);
                }
                else {
                    break;
                }
                i++;
            }

            HintsStorage.HintCountChanged += OnHintCountChanged;
        }

        private void OnHintCountChanged((Hint hint, int prevCount, int currentCount, int delta) args) {
            if (_views.TryGetValue(args.hint, out var view)) {
                var countInfo = GetCountInfo((args.hint, args.currentCount));
                view.UpdateCount(countInfo);
            }
        }

        private string GetCountInfo((Hint hint, int count) pair) {
            var count = pair.count;
            return count > 0 ? count.ToString() : "ADS";
        }

        private void OnEnable() {
            _firstHintView.Clicked += OnFirstHintClicked;
            _secondHintView.Clicked += OnSecondHintClicked;
            if (_views.Count > 0) {
                HintsStorage.HintCountChanged += OnHintCountChanged;
            }
        }

        private void OnFirstHintClicked() {
            HintClick?.Invoke(_firstHintView.Data.hint);
        }

        private void OnSecondHintClicked() {
            HintClick?.Invoke(_secondHintView.Data.hint);
        }

        private void OnDisable() {
            _firstHintView.Clicked -= OnFirstHintClicked;
            _secondHintView.Clicked -= OnSecondHintClicked;
            if (_views.Count > 0) {
                HintsStorage.HintCountChanged -= OnHintCountChanged;
            }
        }
    }
}
