using System;
using System.Collections.Generic;
using UnityEngine;

namespace Presentors {
    public class LetterSelector : MonoBehaviour {
        [SerializeField] private LetterSelectorView _view = null;

        public event Action<char[]> CombinationSelected;

        private char[] _letters;
        private List<char> _combination;
        private int _lastIndex = -1;

        private void Awake() {
            _view.LetterSelected += OnLetterSelected;
            _view.SelectingEnded += OnSelectingEnded;
        }

        public void Init(char[] letters) {
            _letters = letters;
            _combination = new List<char>(letters.Length);
            _view.Init(letters);
        }

        public void ShuflLetters() {
            _view.ShuflLetters();
        }

        private void OnSelectingEnded() {
            if (_combination.Count > 1) {
                CombinationSelected?.Invoke(_combination.ToArray());
            }

            _combination.Clear();
            _lastIndex = -1;
        }

        private void OnLetterSelected(int index) {
            if (_lastIndex != index) {
                _lastIndex = index;
                _combination.Add(_letters[index]);
            }
        }
    }
}