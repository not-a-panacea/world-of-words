using Model;
using System;
using System.Collections.Generic;
using UnityEngine;
using Views;

namespace Presentors {
    public class CategoriesList : MonoBehaviour {
        [SerializeField] private CategoriesListView _view = null;

        public event Action<Category> CategoryClicked;

        private void Awake() {
            _view.CategoryClicked += OnCategoryClicked;
        }

        private void OnCategoryClicked(CategoryView view) {
            CategoryClicked?.Invoke(view.Data.Category);
        }

        public void Init(IEnumerable<CategoryData> categories) {
            _view.Show(categories);
        }

        public void OnCategoryOpened(Category category) {
            _view.OnCategoryOpened(category);
        }
    }
}
